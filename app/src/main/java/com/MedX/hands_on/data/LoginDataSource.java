package com.MedX.hands_on.data;

import com.MedX.hands_on.data.model.LoggedInUser;

import java.io.IOException;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {

    public Result<LoggedInUser> login(String username, String password) {

        try {
            // TODO: handle loggedInUser authentication

            if( !username.equals("test") || !password.equals("testing"))
            {
                int a = 38 / 0;
                a++;
            }
            LoggedInUser testUser = new LoggedInUser(java.util.UUID.randomUUID().toString(), "Test User");
            return new Result.Success<>(testUser);

            /*LoggedInUser fakeUser =
                    new LoggedInUser(
                            java.util.UUID.randomUUID().toString(),
                            "Jane Doe");
            return new Result.Success<>(fakeUser);*/
        } catch (Exception e) {
            return new Result.Error(new IOException("Error logging in", e));
        }
    }

    public void logout() {
        // TODO: revoke authentication
    }
}
