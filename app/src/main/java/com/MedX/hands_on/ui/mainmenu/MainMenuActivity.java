package com.MedX.hands_on.ui.mainmenu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.MedX.hands_on.R;
import com.MedX.hands_on.ui.drugLookup.DrugLookupActivity;
import com.MedX.hands_on.ui.PatientInfo.PatientInfoFieldsActivity;
import com.MedX.hands_on.ui.PatientInfo.PatientSearchActivity;
import com.MedX.hands_on.ui.Settings.SettingsActivity;

import androidx.appcompat.app.AppCompatActivity;

public class MainMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        //create buttons
        final Button drugLookupButton = findViewById(R.id.drugLookupButton);
        final Button newPatientButton = findViewById(R.id.newPatientButton);
        final Button returningPatientButton = findViewById(R.id.returningPatientButton);
        final Button settingsButton = findViewById(R.id.settingsButton);

        /* //The appbar is causing the app to crash after logging in. Commented out, for now, to continue development.

        //BottomNavigationView navView = findViewById(R.id.nav_view);
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
                .build();

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
         */
    }

    //TODO: make add patient/returning patient start for result.
    public void goAddNewPatient(View v) {
        Intent intent = new Intent(this, PatientInfoFieldsActivity.class);
        startActivity(intent);
    }

    public void goReturningPatient(View v){
        Intent intent = new Intent(this, PatientSearchActivity.class);
        startActivity(intent);
    }

    public void goDrugLookup(View v){
        Intent intent = new Intent(this, DrugLookupActivity.class);
        startActivity(intent);
    }

    public void goSettings(View v){
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

}
