package com.MedX.hands_on.ui.PatientInfo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.MedX.hands_on.R;

import androidx.appcompat.app.AppCompatActivity;

public class IntegumentaryActivity extends AppCompatActivity {

    CheckBox myWarm;
    CheckBox myDry;
    CheckBox myCool;
    CheckBox myChills;
    CheckBox myIntact;
    CheckBox myWound;
    CheckBox myUlcer;
    CheckBox myIncision;
    CheckBox myRash;
    CheckBox myItching;
    CheckBox myTurgor;
    CheckBox myWNLInteg;
    EditText myIntegOther;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.integumentary_layout);

        //connect variables to buttons
        myWarm = findViewById(R.id.warmBox);
        myDry = findViewById(R.id.dryBox);
        myCool = findViewById(R.id.coolBox);
        myChills = findViewById(R.id.chillBox);
        myIntact = findViewById( R.id.intactBox);
        myWound = findViewById(R.id.woundBox);
        myUlcer = findViewById(R.id.ulcerBox);
        myIncision = findViewById(R.id.incisionIntegBox);
        myRash = findViewById(R.id.rashBox);
        myItching = findViewById(R.id.itchingBox);
        myTurgor = findViewById(R.id.turgorBox);
        myWNLInteg = findViewById(R.id.wnlIntegBox);
        myIntegOther = findViewById(R.id.integumentaryOtherText);

        //load saved state
        Integumentary integumentary;
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getStringArray("preExistingInteg") != null){
            integumentary = new Integumentary();
            integumentary.massSet(extras.getStringArray("preExistingInteg"));
            myWarm.setChecked(integumentary.getWarm());
            myDry.setChecked(integumentary.getDry());
            myCool.setChecked(integumentary.getCool());
            myChills.setChecked(integumentary.getChills());
            myIntact.setChecked(integumentary.getIntact());
            myWound.setChecked(integumentary.getWound());
            myUlcer.setChecked(integumentary.getUlcer());
            myIncision.setChecked(integumentary.getIncision());
            myRash.setChecked(integumentary.getRash());
            myItching.setChecked(integumentary.getItching());
            myTurgor.setChecked(integumentary.getTurgor());
            myWNLInteg.setChecked(integumentary.getWNL());
            if(myIntegOther != null){ myIntegOther.setText(integumentary.getOtherText()); }
        }
    }

    public void submitIntegumentary(View v){
        String[] returnData = new String[13];
        returnData[0] = myWarm.isChecked() ? "1" : "0";
        returnData[1] = myDry.isChecked() ? "1" : "0";
        returnData[2] = myCool.isChecked() ? "1" : "0";
        returnData[3] = myChills.isChecked() ? "1" : "0";
        returnData[4] = myIntact.isChecked() ? "1" : "0";
        returnData[5] = myWound.isChecked() ? "1" : "0";
        returnData[6] = myUlcer.isChecked() ? "1" : "0";
        returnData[7] = myIncision.isChecked() ? "1" : "0";
        returnData[8] = myRash.isChecked() ? "1" : "0";
        returnData[9] = myItching.isChecked() ? "1" : "0";
        returnData[10] = myTurgor.isChecked() ? "1" : "0";
        returnData[11] = myWNLInteg.isChecked() ? "1" : "0";
        returnData[12] = myIntegOther.getText().toString();

        Intent returnIntent = new Intent();
        returnIntent.putExtra("returnDataInteg", returnData);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

}
