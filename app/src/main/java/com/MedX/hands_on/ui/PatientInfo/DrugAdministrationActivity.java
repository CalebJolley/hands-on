package com.MedX.hands_on.ui.PatientInfo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.ImageButton;
import java.util.Date;
import java.util.Calendar;
import java.util.TimeZone;

import com.MedX.hands_on.R;

public class DrugAdministrationActivity extends AppCompatActivity {

    int drugCount = 0;
    TableLayout ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drug_administration);
        ll = (TableLayout) findViewById(R.id.DrugAdminTable);

        Bundle extras = getIntent().getExtras();
        drugCount = Integer.parseInt(extras.getString("numDrugs", "0")); //extras.getIntExtra("numDrugs", 0);
        if (drugCount != 0){
            DrugAdministration drug;
            String drugNum = "preExistingDrug";
            for (int i = 0; i < drugCount; i++) {
                drug = new DrugAdministration();
                drug.massSet(extras.getStringArray((drugNum + Integer.toString(i))));

                TableRow row = new TableRow(this);
                TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                row.setLayoutParams(lp);

                EditText name = new EditText(this);
                name.setText(drug.getName());
                EditText ID = new EditText(this);
                ID.setText(drug.getID());
                EditText strength = new EditText(this);
                strength.setText(drug.getStrength());
                EditText date = new EditText(this);
                date.setText(drug.getDate());
                EditText dosage = new EditText(this);
                dosage.setText(drug.getDosage());
                EditText route = new EditText(this);
                route.setText(drug.getRoute());
                EditText interval = new EditText(this);
                interval.setText(drug.getInterval());
                EditText physician = new EditText(this);
                physician.setText(drug.getPhysician());
                EditText specialInstructions = new EditText(this);
                specialInstructions.setText(drug.getSpecialInstructions());
                EditText otherNotes = new EditText(this);
                otherNotes.setText(drug.getOtherNotes());
                EditText adminDate = new EditText(this);
                adminDate.setText(drug.getAdminDate());
                EditText adminTime = new EditText(this);
                adminTime.setText(drug.getAdminTime());

                row.addView(name);
                row.addView(ID);
                row.addView(strength);
                row.addView(date);
                row.addView(dosage);
                row.addView(route);
                row.addView(interval);
                row.addView(physician);
                row.addView(specialInstructions);
                row.addView(otherNotes);
                row.addView(adminDate);
                row.addView(adminTime);

                Button adminBtn = new Button(this);
                adminBtn.setText("Administer Drug");
                final int j = i;
                adminBtn.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    adminDrug(v, j);
                                                }
                                            });
                row.addView(adminBtn);

                ll.addView(row, i);
            }
        }
    }

    public void addDrug(View v) {
        TableRow row = new TableRow(this);
        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
        row.setLayoutParams(lp);

        EditText name = new EditText(this);
        name.setText("Medication Name");
        EditText ID = new EditText(this);
        ID.setText("Medication UPC");
        EditText strength = new EditText(this);
        strength.setText("Strength");
        EditText date = new EditText(this);
        date.setText("Date Prescribed");
        EditText dosage = new EditText(this);
        dosage.setText("Dosage");
        EditText route = new EditText(this);
        route.setText("Route");
        EditText interval = new EditText(this);
        interval.setText("Interval");
        EditText physician = new EditText(this);
        physician.setText("Prescribing Physician");
        EditText specialInstructions = new EditText(this);
        specialInstructions.setText("Special Instructions");
        EditText otherNotes = new EditText(this);
        otherNotes.setText("Other Notes");
        EditText adminDate = new EditText(this);
        adminDate.setText("Date Administered");
        EditText adminTime = new EditText(this);
        adminTime.setText("Time Administered");

        row.addView(name);
        row.addView(ID);
        row.addView(strength);
        row.addView(date);
        row.addView(dosage);
        row.addView(route);
        row.addView(interval);
        row.addView(physician);
        row.addView(specialInstructions);
        row.addView(otherNotes);
        row.addView(adminDate);
        row.addView(adminTime);

        Button adminBtn = new Button(this);
        adminBtn.setText("Administer Drug");
        final int j = drugCount;
        adminBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adminDrug(v, j);
            }
        });
        row.addView(adminBtn);

        ll.addView(row, drugCount);
        drugCount++;
    }

    public void adminDrug(View v, int rowNum) {

        View current = ll.getChildAt(rowNum);
        TableRow row = (TableRow) current;

        Calendar localCalendar = Calendar.getInstance(TimeZone.getDefault());
        int currentDay = localCalendar.get(Calendar.DATE);
        int currentMonth = localCalendar.get(Calendar.MONTH) + 1;
        int currentYear = localCalendar.get(Calendar.YEAR);
        int currentHour = localCalendar.get(Calendar.HOUR_OF_DAY);
        int currentMinute = localCalendar.get(Calendar.MINUTE);

        ((EditText)row.getChildAt(10)).setText(currentMonth + "/" + currentDay + "/" + currentYear);
        ((EditText)row.getChildAt(11)).setText(currentHour + ":" + currentMinute);
    }

    public void submitDrug(View v) {

        Intent returnIntent = new Intent();
        returnIntent.putExtra("numDrugs", drugCount);

        for (int i = 0; i < drugCount; i++) {
            View current = ll.getChildAt(i);
            TableRow row = (TableRow) current;
            String returnData[] = new String[12];
            returnData[0] = ((EditText)(row.getChildAt(0))).getText().toString();
            returnData[1] = ((EditText)(row.getChildAt(1))).getText().toString();
            returnData[2] = ((EditText)(row.getChildAt(2))).getText().toString();
            returnData[3] = ((EditText)(row.getChildAt(3))).getText().toString();
            returnData[4] = ((EditText)(row.getChildAt(4))).getText().toString();
            returnData[5] = ((EditText)(row.getChildAt(5))).getText().toString();
            returnData[6] = ((EditText)(row.getChildAt(6))).getText().toString();
            returnData[7] = ((EditText)(row.getChildAt(7))).getText().toString();
            returnData[8] = ((EditText)(row.getChildAt(8))).getText().toString();
            returnData[9] = ((EditText)(row.getChildAt(9))).getText().toString();
            returnData[10] = ((EditText)(row.getChildAt(10))).getText().toString();
            returnData[11] = ((EditText)(row.getChildAt(11))).getText().toString();

            returnIntent.putExtra(("returnDataDrug" + Integer.toString(i)), returnData);
        }

        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}

