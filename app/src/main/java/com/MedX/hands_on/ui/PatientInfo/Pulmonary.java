package com.MedX.hands_on.ui.PatientInfo;

import android.util.Log;

import java.io.Serializable;

public class Pulmonary implements Serializable {

    //variables
    private boolean lungs;
    private boolean sobDizzy;
    private boolean cough;
    private boolean sputum;
    private boolean oxygen;
    private boolean wnl;
    private String otherText;

    public void Pulmonary(){
        lungs = false;
        sobDizzy = false;
        cough = false;
        sputum = false;
        oxygen = false;
        wnl = false;
        otherText = "";
    }

    public void massSet(String[] data){
        if (data[0].equals("1")) lungs = true; else lungs = false;
        if (data[1].equals("1")) sobDizzy = true; else sobDizzy = false;
        if (data[2].equals("1")) cough = true; else sputum = false;
        if (data[3].equals("1"))  sputum= true; else sputum = false;
        if (data[4].equals("1")) oxygen = true; else oxygen = false;
        if (data[5].equals("1")) wnl = true; else wnl = false;
        if(data[6] == null || data[6].isEmpty()){ otherText = ""; } else { otherText = data[6]; }
    }

    public String[] toStringArray(){

        String[] data = new String[7];

        data[0] = lungs ? "1" : "0";
        data[1] = sobDizzy ? "1" : "0";
        data[2] = cough ? "1" : "0";
        data[3] = sputum ? "1" : "0";
        data[4] = oxygen ? "1" : "0";
        data[5] = wnl ? "1" : "0";
        if(otherText == null || otherText.isEmpty()){ data[6] = ""; } else { data[6] = otherText; }

        return data;
    }

    //getters
    public boolean getLungs(){
        return lungs;
    }
    public boolean getSobDizzy(){
        return sobDizzy;
    }
    public boolean getCough(){
        return cough;
    }
    public boolean getSputum(){
        return sputum;
    }
    public boolean getOxygen(){
        return oxygen;
    }
    public boolean getWnlPul(){
        return wnl;
    }
    public String getOtherText(){
        return otherText;
    }

    //setters
    public void setLungs(boolean b){
        lungs = b;
    }
    public void setSobDizzy(boolean b){
        sobDizzy = b;
    }
    public void setCough(boolean b){
        cough = b;
    }
    public void setSputum(boolean b){
        sputum = b;
    }
    public void setOxygen(boolean b){
        oxygen = b;
    }
    public void setWnl(boolean b){
        wnl = b;
    }
    public void setOtherText(String s){
        otherText = s;
    }

}
