package com.MedX.hands_on.ui.PatientInfo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.MedX.hands_on.R;

import androidx.appcompat.app.AppCompatActivity;

public class TechniqueActivity extends AppCompatActivity {

    //variable dec
    CheckBox myPrecautions;
    CheckBox myAsepticTech;
    CheckBox mySharpObjDisp;
    CheckBox myWasteDisp;
    CheckBox myGlucometer;
    EditText myGlucometerCalibr;
    EditText myTechniqueOther;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.technique_layout);

        //connect variables to buttons
        myPrecautions = findViewById(R.id.precautionBox);
        myAsepticTech = findViewById(R.id.asepticBox);
        mySharpObjDisp = findViewById(R.id.sharpObjDispBox);
        myWasteDisp = findViewById(R.id.wasteDispBox);
        myGlucometer = findViewById(R.id.glucometerBox);
        myGlucometerCalibr = findViewById(R.id.glucometerCalibrText);
        myTechniqueOther = findViewById(R.id.techniqueOtherText);


        //load saved state
        Technique technique;
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getStringArray("preExistingTechnique") != null){
            technique = new Technique();
            technique.massSet(extras.getStringArray("preExistingTechnique"));
            myPrecautions.setChecked(technique.getPrecautions());
            myAsepticTech.setChecked(technique.getAsepticTech());
            mySharpObjDisp.setChecked(technique.getSharpObjDisp());
            myWasteDisp.setChecked(technique.getWasteDisp());
            myGlucometer.setChecked(technique.getGlucometer());
            if (myGlucometerCalibr != null) { myGlucometerCalibr.setText(technique.getGlucometerCalib());}
            if (myTechniqueOther != null) { myTechniqueOther.setText(technique.getOtherText());}
        }
    }

    public void submitTechnique(View v){
        String[] returnData = new String[7];

        returnData[0] = myPrecautions.isChecked() ? "1" : "0";
        returnData[1] = myAsepticTech.isChecked() ? "1" : "0";
        returnData[2] = mySharpObjDisp.isChecked() ? "1" : "0";
        returnData[3] = myWasteDisp.isChecked() ? "1" : "0";
        returnData[4] = myGlucometer.isChecked() ? "1" : "0";
        returnData[5] = myGlucometerCalibr.getText().toString();
        returnData[6] = myTechniqueOther.getText().toString();

        Intent returnIntent = new Intent();
        returnIntent.putExtra("returnDataTechnique", returnData);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

}
