package com.MedX.hands_on.ui.PatientInfo;

import java.io.Serializable;

public class MusculoSkeletal implements Serializable {

    //variable dec
    private boolean poorBalance;
    private boolean limitedMovement;
    private boolean chairBound;
    private boolean bedBound;
    private String walksWith;
    private boolean contracture;
    private boolean paralysis;
    private boolean wnl;
    private String otherText;

    public void MusculoSkeletal(){

        poorBalance = false;
        limitedMovement = false;
        chairBound = false;
        bedBound = false;
        walksWith = "";
        contracture = false;
        paralysis = false;
        wnl = false;
        otherText = "";
    }

    public void massSet(String[] data){
        if (data[0].equals("1")) poorBalance = true; else poorBalance = false;
        if (data[1].equals("1")) limitedMovement = true; else limitedMovement = false;
        if (data[2].equals("1")) chairBound = true; else chairBound = false;
        if (data[3].equals("1")) bedBound= true; else bedBound = false;
        if(data[4] == null || data[4].isEmpty()){ walksWith = ""; } else { walksWith = data[4]; }
        if (data[5].equals("1")) contracture = true; else contracture = false;
        if (data[6].equals("1")) paralysis = true; else paralysis = false;
        if (data[7].equals("1")) wnl = true; else wnl = false;
        if(data[8] == null || data[8].isEmpty()){ otherText = ""; } else { otherText = data[8]; }
    }

    public String[] toStringArray(){

        String[] data = new String[9];

        data[0] = poorBalance ? "1" : "0";
        data[1] = limitedMovement ? "1" : "0";
        data[2] = chairBound ? "1" : "0";
        data[3] = bedBound ? "1" : "0";
        if(walksWith == null || walksWith.isEmpty()){ data[4] = ""; } else { data[4] = walksWith; }
        data[5] = contracture ? "1" : "0";
        data[6] = paralysis ? "1" : "0";
        data[7] = wnl ? "1" : "0";
        if(otherText == null || otherText.isEmpty()){ data[8] = ""; } else { data[8] = otherText; }

        return data;
    }

    //getters
    public boolean getPoorBalance() {
        return poorBalance;
    }
    public boolean getLimitedMovement() {
        return limitedMovement;
    }
    public boolean getChairBound() {
        return chairBound;
    }
    public boolean getBedBound() {
        return bedBound;
    }
    public String getWalksWith() {
        return walksWith;
    }
    public boolean getContracture() {
        return contracture;
    }
    public boolean getParalysis() {
        return paralysis;
    }
    public boolean getWNL() {
        return wnl;
    }
    public String getOtherText() {
        return otherText;
    }

    //setters
    public void setPoorBalance(boolean poorBalance) {
        this.poorBalance = poorBalance;
    }
    public void setLimitedMovement(boolean limitedMovement) {
        this.limitedMovement = limitedMovement;
    }
    public void setChairBound(boolean chairBound) {
        this.chairBound = chairBound;
    }
    public void setBedBound(boolean bedBound) {
        this.bedBound = bedBound;
    }
    public void setWalksWith(String walksWith) {
        this.walksWith = walksWith;
    }
    public void setContracture(boolean contracture) {
        this.contracture = contracture;
    }
    public void setParalysis(boolean paralysis) {
        this.paralysis = paralysis;
    }
    public void setWnl(boolean wnl) {
        this.wnl = wnl;
    }
    public void setOtherText(String otherText) {
        this.otherText = otherText;
    }
}
