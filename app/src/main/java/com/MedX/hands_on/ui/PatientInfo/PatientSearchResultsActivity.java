package com.MedX.hands_on.ui.PatientInfo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.MedX.hands_on.R;

import java.io.Serializable;
import java.util.ArrayList;

public class PatientSearchResultsActivity extends AppCompatActivity {//mergefix commment?

    //variable dec
    private static final String TAG = "PatientSearchResultsAct";
    private ArrayList<String> mDates;
    private EditText patientName;
    private EditText bDay;
    private EditText email;
    private EditText address;
    private EditText phoneNum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.patient_search_results_layout);

        //create ArrayList and connect text boxes.
        mDates = new ArrayList<>();
        patientName = findViewById(R.id.patientSearchResultNameText);
        bDay = findViewById(R.id.patientSearchResultBdayText);
        email = findViewById(R.id.patientSearchResultEmailText);
        address = findViewById(R.id.patientSearchResultAddress);
        phoneNum = findViewById(R.id.patientSearchResultPhoneText);

        initData();
    }

    //load data to display
    private void initData(){
        //TODO: load from database here. Each entry represents a unique PatientInfoFields form.

        //'load' patient info. Hardcoded for testing purposes.
        patientName.setText("Joe Bob");
        bDay.setText("5-9-2020");
        email.setText("NoBodyKnows@ExampleEmail.com");
        address.setText("1337 None-ya road, NoWhereVille, TX, 55555");
        phoneNum.setText("555-555-5555");

        //hardcoded dates represents data loaded from database. Each date is a day this patient has had a form filled out.
        mDates.add("1-5-2020");
        mDates.add("1-20-2020");
        mDates.add("2-7-2020");
        mDates.add("2-21-2020");
        mDates.add("3-3-2020");
        mDates.add("3-19-2020");
        mDates.add("4-05-2020");
        mDates.add("4-22-2020");
        mDates.add("5-8-2020");
        mDates.add("5-16-2020");
        mDates.add("6-4-2020");
        mDates.add("6-20-2020");
        mDates.add("7-5-2020");
        mDates.add("7-23-2020");

        //after loading, initialize the RecyclerView
        initRecyclerView();
    }

    private void initRecyclerView(){
        //set up RecyclerView
        RecyclerView recyclerView = findViewById(R.id.searchResultRecyclerView);
        PatientSearchResultRecyclerAdapter adapter = new PatientSearchResultRecyclerAdapter(this, mDates);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public void endPatientSearchResult(View v){
        //TODO: return something so patientSearch knows it's done.
        finish();
    }

}
