package com.MedX.hands_on.ui.PatientInfo;

public class GastroIntestinal {

    //variable dec
    private boolean bowelSounds;
    private boolean abdomenSoft;
    private boolean abdomenTender;
    private boolean distended;
    private boolean nausea;
    private boolean vomiting;
    private boolean npo;
    private boolean diarrhea;
    private boolean constipation;
    private boolean incontinence;
    private boolean ostomy;
    private boolean peg;
    private boolean feeding;
    private boolean flushing;
    private String lastBM;
    private boolean wnl;
    private String otherText;

    public void GastroIntestinal(){
        bowelSounds = false;
        abdomenSoft = false;
        abdomenTender = false;
        distended = false;
        nausea = false;
        vomiting = false;
        npo = false;
        diarrhea = false;
        constipation = false;
        incontinence = false;
        ostomy = false;
        peg = false;
        feeding = false;
        flushing = false;
        lastBM = "";
        wnl = false;
        otherText = "";
    }

    public void massSet(String[] data){
        if (data[0].equals("1")) bowelSounds = true; else bowelSounds = false;
        if (data[1].equals("1")) abdomenSoft = true; else abdomenSoft = false;
        if (data[2].equals("1")) abdomenTender = true; else abdomenTender = false;
        if (data[3].equals("1")) distended= true; else distended = false;
        if (data[4].equals("1")) nausea = true; else nausea = false;
        if (data[5].equals("1")) vomiting = true; else vomiting = false;
        if (data[6].equals("1")) npo = true; else npo = false;
        if (data[7].equals("1")) diarrhea = true; else diarrhea = false;
        if (data[8].equals("1")) constipation = true; else constipation = false;
        if (data[9].equals("1")) incontinence = true; else incontinence = false;
        if (data[10].equals("1")) ostomy = true; else ostomy = false;
        if (data[11].equals("1")) peg = true; else peg = false;
        if (data[12].equals("1")) feeding = true; else feeding = false;
        if (data[13].equals("1")) flushing = true; else flushing = false;
        if(data[14] == null || data[14].isEmpty()){ lastBM = ""; } else { lastBM = data[14]; }
        if (data[15].equals("1")) wnl = true; else wnl = false;
        if(data[16] == null || data[15].isEmpty()){ otherText = ""; } else { otherText = data[16]; }
    }

    public String[] toStringArray(){

        String[] data = new String[17];

        data[0] = bowelSounds ? "1" : "0";
        data[1] = abdomenSoft ? "1" : "0";
        data[2] = abdomenTender ? "1" : "0";
        data[3] = distended ? "1" : "0";
        data[4] = nausea ? "1" : "0";
        data[5] = vomiting ? "1" : "0";
        data[6] = npo ? "1" : "0";
        data[7] = diarrhea ? "1" : "0";
        data[8] = constipation ? "1" : "0";
        data[9] = incontinence ? "1" : "0";
        data[10] = ostomy ? "1" : "0";
        data[11] = peg ? "1" : "0";
        data[12] = feeding ? "1" : "0";
        data[13] = flushing ? "1" : "0";
        if(lastBM == null || lastBM.isEmpty()){ data[14] = ""; } else { data[14] = lastBM; }
        data[15] = wnl ? "1" : "0";
        if(otherText == null || otherText.isEmpty()){ data[16] = ""; } else { data[16] = otherText; }

        return data;
    }

    //getters
    public boolean getBowelSounds() { return bowelSounds; }
    public boolean getAbdomenSoft() { return abdomenSoft; }
    public boolean getAbdomenTender() { return abdomenTender; }
    public boolean getDistended() { return distended; }
    public boolean getNausea() { return nausea; }
    public boolean getVomiting() { return vomiting; }
    public boolean getNpo() { return npo; }
    public boolean getDiarrhea() { return diarrhea; }
    public boolean getConstipation() { return constipation; }
    public boolean getIncontinence() { return incontinence; }
    public boolean getOstomy() { return ostomy; }
    public boolean getPeg() { return peg; }
    public boolean getFeeding() { return feeding; }
    public boolean getFlushing() { return flushing; }
    public String getLastBM() { return lastBM; }
    public boolean getWnl() { return wnl; }
    public String getOtherText() { return otherText; }

    //setters
    public void setBowelSounds(boolean bowelSounds) { this.bowelSounds = bowelSounds; }
    public void setAbdomenSoft(boolean abdomenSoft) { this.abdomenSoft = abdomenSoft; }
    public void setAbdomenTender(boolean abdomenTender) { this.abdomenTender = abdomenTender; }
    public void setDistended(boolean distended) { this.distended = distended; }
    public void setNausea(boolean nausea) { this.nausea = nausea; }
    public void setVomiting(boolean vomiting) { this.vomiting = vomiting; }
    public void setNpo(boolean npo) { this.npo = npo; }
    public void setDiarrhea(boolean diarrhea) { this.diarrhea = diarrhea; }
    public void setConstipation(boolean constipation) { this.constipation = constipation; }
    public void setIncontinence(boolean incontinence) { this.incontinence = incontinence; }
    public void setOstomy(boolean ostomy) { this.ostomy = ostomy; }
    public void setPeg(boolean peg) { this.peg = peg; }
    public void setFeeding(boolean feeding) { this.feeding = feeding; }
    public void setFlushing(boolean flushing) { this.flushing = flushing; }
    public void setLastBM(String lastBM) { this.lastBM = lastBM; }
    public void setWnl(boolean wnl) { this.wnl = wnl; }
    public void setOtherText(String otherText) { this.otherText = otherText; }

}
