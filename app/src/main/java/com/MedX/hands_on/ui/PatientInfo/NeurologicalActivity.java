package com.MedX.hands_on.ui.PatientInfo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.MedX.hands_on.R;

import androidx.appcompat.app.AppCompatActivity;

public class NeurologicalActivity extends AppCompatActivity {

    //variable dec
    CheckBox myHeadache;
    CheckBox mySyncpe;
    CheckBox myVertigo;
    CheckBox myEqualGrasp;
    CheckBox myUnequalGrasp;
    CheckBox myMovement;
    CheckBox myEqualPupils;
    CheckBox myUnequalPupils;
    CheckBox myHandTremors;
    CheckBox myAphasia;
    CheckBox myDysphasia;
    CheckBox mySpeechImpair;
    CheckBox myHearingImpair;
    CheckBox myVisualImpair;
    CheckBox myWNLNeuro;
    EditText myNeuroOther;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.neurological_layout);

        //connect variables to buttons
        myHeadache = findViewById(R.id.headacheBox);
        mySyncpe = findViewById(R.id.syncpeBox);
        myVertigo = findViewById(R.id.vertigoBox);
        myEqualGrasp = findViewById(R.id.graspEqualBox);
        myUnequalGrasp = findViewById(R.id.graspUnequalBox);
        myMovement = findViewById(R.id.movementBox);
        myEqualPupils = findViewById(R.id.pupilsEqualBox);
        myUnequalPupils = findViewById(R.id.pupilsUnequalBox);
        myHandTremors = findViewById(R.id.handTremorsBox);
        myAphasia = findViewById(R.id.aphasiaBox);
        myDysphasia = findViewById(R.id.dysphasiaBox);
        mySpeechImpair = findViewById(R.id.speechImpairBox);
        myHearingImpair = findViewById(R.id.hearingImpairBox);
        myVisualImpair = findViewById(R.id.visualImpairBox);
        myWNLNeuro = findViewById(R.id.wnlNeuroBox);
        myNeuroOther = findViewById(R.id.neuroOtherText);

        //load saved state
        Neurological neurological;
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getStringArray("preExistingNeuro") != null){
            neurological = new Neurological();
            neurological.massSet(extras.getStringArray("preExistingNeuro"));
            myHeadache.setChecked(neurological.getHeadache());
            mySyncpe.setChecked(neurological.getSyncpe());
            myVertigo.setChecked(neurological.getVertigo());
            myEqualGrasp.setChecked(neurological.getEqualGrasp());
            myUnequalGrasp.setChecked(neurological.getUnEqualGrasp());
            myMovement.setChecked(neurological.getMovement());
            myEqualPupils.setChecked(neurological.getEqualPupils());
            myUnequalPupils.setChecked(neurological.getUnEqualPupils());
            myHandTremors.setChecked(neurological.getHandTremors());
            myAphasia.setChecked(neurological.getAphasia());
            myDysphasia.setChecked(neurological.getDysphasia());
            mySpeechImpair.setChecked(neurological.getSpeechImpair());
            myHearingImpair.setChecked(neurological.getHearingImpair());
            myVisualImpair.setChecked(neurological.getVisualImpair());
            myWNLNeuro.setChecked(neurological.getWnl());
            if(myNeuroOther != null){ myNeuroOther.setText(neurological.getOtherText()); }
        }
    }

    public void submitNeuro(View v){
        String[] returnData = new String[16];
        returnData[0] = myHeadache.isChecked() ? "1" : "0";
        returnData[1] = mySyncpe.isChecked() ? "1" : "0";
        returnData[2] = myVertigo.isChecked() ? "1" : "0";
        returnData[3] = myEqualGrasp.isChecked() ? "1" : "0";
        returnData[4] = myUnequalGrasp.isChecked() ? "1" : "0";
        returnData[5] = myMovement.isChecked() ? "1" : "0";
        returnData[6] = myEqualPupils.isChecked() ? "1" : "0";
        returnData[7] = myUnequalPupils.isChecked() ? "1" : "0";
        returnData[8] = myHandTremors.isChecked() ? "1" : "0";
        returnData[9] = myAphasia.isChecked() ? "1" : "0";
        returnData[10] = myDysphasia.isChecked() ? "1" : "0";
        returnData[11] = mySpeechImpair.isChecked() ? "1" : "0";
        returnData[12] = myHearingImpair.isChecked() ? "1" : "0";
        returnData[13] = myVisualImpair.isChecked() ? "1" : "0";
        returnData[14] = myWNLNeuro.isChecked() ? "1" : "0";
        returnData[15] = myNeuroOther.getText().toString();

        Intent returnIntent = new Intent();
        returnIntent.putExtra("returnDataNeuro", returnData);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

}