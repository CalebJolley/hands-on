package com.MedX.hands_on.ui.PatientInfo;

public class Pain {

    //variable dec
    private boolean noPain;
    private boolean notDailyPain;
    private boolean dailyPain;
    private boolean constantPain;
    private String painLevel;
    private String painSite;
    private boolean relievedMedYes;
    private boolean relievedMedNo;
    private String otherText;


    public void Pain(){
        noPain = false;
        notDailyPain = false;
        dailyPain = false;
        constantPain = false;
        painLevel = "";
        painSite = "";
        relievedMedYes = false;
        relievedMedNo = false;
        otherText = "";
    }

    public void massSet(String[] data){
        if (data[0].equals("1")) noPain = true; else noPain = false;
        if (data[1].equals("1")) notDailyPain = true; else notDailyPain = false;
        if (data[2].equals("1")) dailyPain = true; else dailyPain = false;
        if (data[3].equals("1")) constantPain= true; else constantPain = false;
        if(data[4] == null || data[4].isEmpty()){ painLevel = ""; } else { painLevel = data[4]; }
        if(data[5] == null || data[5].isEmpty()){ painSite = ""; } else { painSite = data[5]; }
        if (data[6].equals("1")) relievedMedYes = true; else relievedMedYes = false;
        if (data[7].equals("1")) relievedMedNo = true; else relievedMedNo = false;
        if(data[8] == null || data[8].isEmpty()){ otherText = ""; } else { otherText = data[8]; }
    }

    public String[] toStringArray(){

        String[] data = new String[9];

        data[0] = noPain ? "1" : "0";
        data[1] = notDailyPain ? "1" : "0";
        data[2] = dailyPain ? "1" : "0";
        data[3] = constantPain ? "1" : "0";
        if (painLevel == null || painLevel.isEmpty()){ data[4] = ""; } else { data[4] = painLevel; }
        if (painSite == null || painSite.isEmpty()){ data[5] = ""; } else { data[5] = painSite; }
        data[6] = relievedMedYes ? "1" : "0";
        data[7] = relievedMedNo ? "1" : "0";
        if (otherText == null || otherText.isEmpty()){ data[8] = ""; } else { data[8] = otherText; }

        return data;
    }

    //getters
    public boolean getNoPain() {
        return noPain;
    }
    public boolean getNotDailyPain() {
        return notDailyPain;
    }
    public boolean getDailyPain() {
        return dailyPain;
    }
    public boolean getConstantPain() {
        return constantPain;
    }
    public String getPainLevel() {
        return painLevel;
    }
    public String getPainSite() {
        return painSite;
    }
    public boolean getRelievedMedYes() {
        return relievedMedYes;
    }
    public boolean getRelievedMedNo() {
        return relievedMedNo;
    }
    public String getOtherText() {
        return otherText;
    }

    //setters
    public void setNoPain(boolean noPain) {
        this.noPain = noPain;
    }
    public void setNotDailyPain(boolean notDailyPain) {
        this.notDailyPain = notDailyPain;
    }
    public void setDailyPain(boolean dailyPain) {
        this.dailyPain = dailyPain;
    }
    public void setConstantPain(boolean constantPain) {
        this.constantPain = constantPain;
    }
    public void setPainLevel(String painLevel) {
        this.painLevel = painLevel;
    }
    public void setPainSite(String painSite) {
        this.painSite = painSite;
    }
    public void setRelievedMedYes(boolean relievedMedYes) {
        this.relievedMedYes = relievedMedYes;
    }
    public void setRelievedMedNo(boolean relievedMedNo) {
        this.relievedMedNo = relievedMedNo;
    }
    public void setOtherText(String otherText) {
        this.otherText = otherText;
    }




}
