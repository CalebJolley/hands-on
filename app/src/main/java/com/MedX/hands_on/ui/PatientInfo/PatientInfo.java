package com.MedX.hands_on.ui.PatientInfo;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class PatientInfo implements Serializable { //mergefix comment?

    //private static final String TAG = "PatientInfo Extras";
    //variables
    private Cardiovascular cardiovascular;
    private Pulmonary pulmonary;
    private Integumentary integumentary;
    private MusculoSkeletal musculoSkeletal;
    private GastroIntestinal gastroIntestinal;
    private Genitourinary genitourinary;
    private Neurological neurological;
    private Mental mental;
    private Pain pain;
    private Intervention intervention;
    private Technique technique;
    private Infusion infusion;
    private ArrayList drugs;
    private Date date;
    private String patientName;
    private String patientEmail;
    private String patientPhoneNum;
    private String patientAddress;
    private String patientBirthDate;
    private String visitType;

    public void PatientInfo(){
        //default to false/empty
        cardiovascular = new Cardiovascular();
        pulmonary = new Pulmonary();
        integumentary = new Integumentary();
        musculoSkeletal = new MusculoSkeletal();
        gastroIntestinal = new GastroIntestinal();
        genitourinary = new Genitourinary();
        neurological = new Neurological();
        mental = new Mental();
        pain = new Pain();
        intervention = new Intervention();
        technique = new Technique();
        infusion = new Infusion();
        drugs = new ArrayList();
        date = new Date(); //to track the date of creation, for usage in database.

        patientName = "";
        patientEmail = "";
        patientPhoneNum = "";
        patientAddress = "";
        patientBirthDate = "";
        visitType = "";
    }

    //getters
    public Cardiovascular getCardiovascular() {
        return cardiovascular;
    }
    public Pulmonary getPulmonary() {
        return pulmonary;
    }
    public Integumentary getIntegumentary() {
        return integumentary;
    }
    public MusculoSkeletal getMusculoSkeletal() {
        return musculoSkeletal;
    }
    public GastroIntestinal getGastroIntestinal() {return gastroIntestinal; }
    public Genitourinary getGenitourinary() { return genitourinary; }
    public Neurological getNeurological() {
        return neurological;
    }
    public Mental getMental(){ return mental; }
    public Pain getPain() { return pain; }
    public Intervention getIntervention() {
        return intervention;
    }
    public Technique getTechnique() {return technique; }
    public Infusion getInfusion() {return infusion; }
    public ArrayList getDrugs() {return drugs; }
    public String getPatientName() {
        return patientName;
    }
    public String getPatientEmail() {
        return patientEmail;
    }
    public String getPatientPhoneNum() {
        return patientPhoneNum;
    }
    public String getPatientAddress() {
        return patientAddress;
    }
    public String getPatientBirthDate() {
        return patientBirthDate;
    }
    public String getVisitType() { return visitType; }

    //setters
    public void setCardiovascular(Cardiovascular c){
        cardiovascular = c;
    }
    public void setPulmonary(Pulmonary p){
        pulmonary = p;
    }
    public void setIntegumentary(Integumentary i){
        integumentary = i;
    }
    public void setMusculoSkeletal(MusculoSkeletal m){
        musculoSkeletal = m;
    }
    public void setGastroIntestinal(GastroIntestinal g) { gastroIntestinal = g; }
    public void setGenitourinary(Genitourinary g) { genitourinary = g; }
    public void setNeurological(Neurological n){ neurological = n; }
    public void setMental(Mental m) { mental = m; }
    public void setPain(Pain p) { pain = p; }
    public void setIntervention(Intervention i){ intervention = i; }
    public void setTechnique(Technique t ) {technique = t; }
    public void setInfusion(Infusion i) {infusion = i; }
    public void setDrugs(ArrayList d) {drugs = d;}
    public void setPatientName(String s) { patientName = s; }
    public void setPatientEmail(String s) { patientEmail = s; }
    public void setPatientPhoneNum(String s) { patientPhoneNum = s; }
    public void setPatientAddress(String s) { patientAddress = s; }
    public void setPatientBirthDate(String s) { patientBirthDate = s; }
    public void setVisitType(String s) { visitType = s; }

}
