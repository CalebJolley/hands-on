package com.MedX.hands_on.ui.PatientInfo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.MedX.hands_on.R;

import androidx.appcompat.app.AppCompatActivity;

public class InterventionActivity extends AppCompatActivity {

    //variable dec
    CheckBox mySkilledAsses;
    CheckBox myFoleyChange;
    CheckBox myIrrigation;
    CheckBox myWound;
    CheckBox myUlcer;
    CheckBox myIncision;
    CheckBox myPrep;
    CheckBox myAdminInsulin;
    CheckBox myIM;
    CheckBox mySQ;
    CheckBox myPEG;
    CheckBox myGTCare;
    CheckBox myDiet;
    CheckBox myMedInstruct;
    CheckBox mySSDiseaseProcess;
    EditText myInterventionOther;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intervention_layout);

        //connect variables to buttons
        mySkilledAsses = findViewById(R.id.skilledAssesBox);
        myFoleyChange = findViewById(R.id.foleyChangeBox);
        myIrrigation = findViewById(R.id.irrigationIntBox);
        myWound = findViewById(R.id.woundIntBox);
        myUlcer = findViewById(R.id.ulcerIntBox);
        myIncision = findViewById(R.id.incisionIntBox);
        myPrep = findViewById(R.id.prepBox);
        myAdminInsulin = findViewById(R.id.adminInsulinBox);
        myIM = findViewById(R.id.imBox);
        mySQ = findViewById(R.id.sqBox);
        myPEG = findViewById(R.id.pegBox);
        myGTCare = findViewById(R.id.gtCareBox);
        myDiet = findViewById(R.id.dietBox);
        myMedInstruct = findViewById(R.id.medInstructionBox);
        mySSDiseaseProcess = findViewById(R.id.ssDiseaseBox);
        myInterventionOther = findViewById(R.id.interventionOtherText);

        //load saved state
        Intervention intervention;
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getStringArray("preExistingIntervention") != null){
            intervention = new Intervention();
            intervention.massSet(extras.getStringArray("preExistingIntervention"));
            mySkilledAsses.setChecked(intervention.getSkilledAssess());
            myFoleyChange.setChecked(intervention.getFoleyChange());
            myIrrigation.setChecked(intervention.getIrrigation());
            myWound.setChecked(intervention.getWound());
            myUlcer.setChecked(intervention.getUlcer());
            myIncision.setChecked(intervention.getIncision());
            myPrep.setChecked(intervention.getPrep());
            myAdminInsulin.setChecked(intervention.getAdminInsulin());
            myIM.setChecked(intervention.getIm());
            mySQ.setChecked(intervention.getSq());
            myPEG.setChecked(intervention.getPeg());
            myGTCare.setChecked(intervention.getGtCare());
            myDiet.setChecked(intervention.getDiet());
            myMedInstruct.setChecked(intervention.getMedInstruct());
            mySSDiseaseProcess.setChecked(intervention.getSsDiseaseProcess());
            if(myInterventionOther != null){ myInterventionOther.setText(intervention.getOtherText()); }
        }
    }

    public void submitIntervention(View v){
        String[] returnData = new String[16];
        returnData[0] = mySkilledAsses.isChecked() ? "1" : "0";
        returnData[1] = myFoleyChange.isChecked() ? "1" : "0";
        returnData[2] = myIrrigation.isChecked() ? "1" : "0";
        returnData[3] = myWound.isChecked() ? "1" : "0";
        returnData[4] = myUlcer.isChecked() ? "1" : "0";
        returnData[5] = myIncision.isChecked() ? "1" : "0";
        returnData[6] = myPrep.isChecked() ? "1" : "0";
        returnData[7] = myAdminInsulin.isChecked() ? "1" : "0";
        returnData[8] = myIM.isChecked() ? "1" : "0";
        returnData[9] = mySQ.isChecked() ? "1" : "0";
        returnData[10] = myPEG.isChecked() ? "1" : "0";
        returnData[11] = myGTCare.isChecked() ? "1" : "0";
        returnData[12] = myDiet.isChecked() ? "1" : "0";
        returnData[13] = myMedInstruct.isChecked() ? "1" : "0";
        returnData[14] = mySSDiseaseProcess.isChecked() ? "1" : "0";
        returnData[15] = myInterventionOther.getText().toString();

        Intent returnIntent = new Intent();
        returnIntent.putExtra("returnDataIntervention", returnData);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

}
