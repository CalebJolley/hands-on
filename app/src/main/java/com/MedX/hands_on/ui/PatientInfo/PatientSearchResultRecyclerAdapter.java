package com.MedX.hands_on.ui.PatientInfo;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.MedX.hands_on.R;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

public class PatientSearchResultRecyclerAdapter extends RecyclerView.Adapter<PatientSearchResultRecyclerAdapter.MyViewHolder>{//mergefix commment?

    private static final String TAG = "PatientSearchResultRecy";
    
    private ArrayList<String> mDates = new ArrayList<>();
    private Context mContext;

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView date;
        RelativeLayout SearchResultLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.patientSearchResultListText);
            SearchResultLayout = itemView.findViewById(R.id.relativeLPatientSearchResult);
        }
    }

    public PatientSearchResultRecyclerAdapter( Context mContext, ArrayList<String> mDates) {
        this.mDates = mDates;
        this.mContext = mContext;
    }

    @Override
    public PatientSearchResultRecyclerAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.patient_search_result_list_layout, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(PatientSearchResultRecyclerAdapter.MyViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called"); //log stuff to find failures.
        holder.date.setText(mDates.get(position));
        holder.SearchResultLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Log.d(TAG, "onClick: clicked on" + mDates.get(position));

                //toast temp to check if click works.
                //TODO: make this load proper PatientInfoFields page, according to the date of the visit.
                //likely done through a specific database query based on patientName (can be passed to this event at start) and the selected date.
                //Toast.makeText(mContext, mDates.get(position), Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(mContext, PatientInfoFieldsActivity.class);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDates.size();
    }

}
