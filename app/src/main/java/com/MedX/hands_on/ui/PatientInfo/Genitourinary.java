package com.MedX.hands_on.ui.PatientInfo;

public class Genitourinary {

    //variable dec
    private boolean burning;
    private boolean dysuria;
    private boolean odor;
    private boolean distention;
    private boolean retention;
    private boolean frequency;
    private boolean urgency;
    private boolean incontinence;
    private boolean hesitance;
    private boolean itching;
    private String color;
    private String catheter;
    private String fr;
    private String cc; //will need to be converted to not-a-string to use.
    private String lastChanged;
    private boolean irrigation;
    private boolean wnl;
    private String otherText;

    public void Genitourinary(){
        burning = false;
        dysuria = false;
        odor = false;
        distention = false;
        retention = false;
        frequency = false;
        urgency = false;
        incontinence = false;
        hesitance = false;
        itching = false;
        color = "";
        catheter = "";
        fr = "";
        cc = "";
        lastChanged = "";
        irrigation = false;
        wnl = false;
        otherText = "";
    }

    public void massSet(String[] data){
        if (data[0].equals("1")) burning = true; else burning = false;
        if (data[1].equals("1")) dysuria = true; else dysuria = false;
        if (data[2].equals("1")) odor = true; else odor = false;
        if (data[3].equals("1")) distention = true; else distention = false;
        if (data[4].equals("1")) retention= true; else retention = false;
        if (data[5].equals("1")) frequency = true; else frequency = false;
        if (data[6].equals("1")) urgency = true; else urgency = false;
        if (data[7].equals("1")) incontinence = true; else incontinence = false;
        if (data[8].equals("1")) hesitance = true; else hesitance = false;
        if (data[9].equals("1")) itching = true; else itching = false;
        if (data[10] == null || data[10].isEmpty()){ color = ""; } else { color = data[10]; }
        if (data[11] == null || data[11].isEmpty()){ catheter = ""; } else { catheter = data[11]; }
        if (data[12] == null || data[12].isEmpty()){ fr = ""; } else { fr = data[12]; }
        if (data[13] == null || data[13].isEmpty()){ cc = ""; } else { cc = data[13]; }
        if (data[14] == null || data[14].isEmpty()){ lastChanged = ""; } else { lastChanged = data[14]; }
        if (data[15].equals("1")) irrigation = true; else irrigation = false;
        if (data[16].equals("1")) wnl = true; else wnl = false;
        if (data[17] == null || data[16].isEmpty()){ otherText = ""; } else { otherText = data[17]; }
    }

    public String[] toStringArray(){

        String[] data = new String[18];

        data[0] = burning ? "1" : "0";
        data[1] = dysuria ? "1" : "0";
        data[2] = odor ? "1" : "0";
        data[3] = distention ? "1" : "0";
        data[4] = retention ? "1" : "0";
        data[5] = frequency ? "1" : "0";
        data[6] = urgency ? "1" : "0";
        data[7] = incontinence ? "1" : "0";
        data[8] = hesitance ? "1" : "0";
        data[9] = itching ? "1" : "0";
        if (color == null || color.isEmpty()){ data[10] = ""; } else { data[10] = color; }
        if (catheter == null || catheter.isEmpty()){ data[11] = ""; } else { data[11] = catheter; }
        if (fr == null || fr.isEmpty()){ data[12] = ""; } else { data[12] = fr; }
        if (cc == null || cc.isEmpty()){ data[13] = ""; } else { data[13] = cc; }
        if (lastChanged == null || lastChanged.isEmpty()){ data[14] = ""; } else { data[14] = lastChanged; }
        data[15] = irrigation ? "1" : "0";
        data[16] = wnl ? "1" : "0";
        if (otherText == null || otherText.isEmpty()){ data[17] = ""; } else { data[17] = otherText; }

        return data;
    }


    //getters
    public boolean getBurning() {
        return burning;
    }
    public boolean getDysuria() {
        return dysuria;
    }
    public boolean getOdor() {
        return odor;
    }
    public boolean getDistention() {
        return distention;
    }
    public boolean getRetention() {
        return retention;
    }
    public boolean getFrequency() {
        return frequency;
    }
    public boolean getUrgency() {
        return urgency;
    }
    public boolean getIncontinence() {
        return incontinence;
    }
    public boolean getHesitance() {
        return hesitance;
    }
    public boolean getItching() {
        return itching;
    }
    public String getColor() {
        return color;
    }
    public String getCatheter() {
        return catheter;
    }
    public String getFr() {
        return fr;
    }
    public String getCc() {
        return cc;
    }
    public String getLastChanged() {
        return lastChanged;
    }
    public boolean getIrrigation() {
        return irrigation;
    }
    public boolean getWnl() {
        return wnl;
    }
    public String getOtherText() {
        return otherText;
    }

    //setters
    public void setBurning(boolean burning) {
        this.burning = burning;
    }
    public void setDysuria(boolean dysuria) {
        this.dysuria = dysuria;
    }
    public void setOdor(boolean odor) {
        this.odor = odor;
    }
    public void setDistention(boolean distention) {
        this.distention = distention;
    }
    public void setRetention(boolean retention) {
        this.retention = retention;
    }
    public void setFrequency(boolean frequency) {
        this.frequency = frequency;
    }
    public void setUrgency(boolean urgency) {
        this.urgency = urgency;
    }
    public void setIncontinence(boolean incontinence) {
        this.incontinence = incontinence;
    }
    public void setHesitance(boolean hesitance) {
        this.hesitance = hesitance;
    }
    public void setItching(boolean itching) {
        this.itching = itching;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public void setCatheter(String catheter) {
        this.catheter = catheter;
    }
    public void setFr(String fr) {
        this.fr = fr;
    }
    public void setCc(String cc) {
        this.cc = cc;
    }
    public void setLastChanged(String lastChanged) {
        this.lastChanged = lastChanged;
    }
    public void setIrrigation(boolean irrigation) {
        this.irrigation = irrigation;
    }
    public void setWnl(boolean wnl) {
        this.wnl = wnl;
    }
    public void setOtherText(String otherText) {
        this.otherText = otherText;
    }
}
