package com.MedX.hands_on.ui.PatientInfo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.MedX.hands_on.R;

import androidx.appcompat.app.AppCompatActivity;

public class MentalActivity extends AppCompatActivity {

    //variable dec
    EditText myOriented;
    CheckBox myForgetful;
    CheckBox myConfused;
    CheckBox myDisoriented;
    CheckBox myLethargic;
    CheckBox myComatose;
    CheckBox myRestless;
    CheckBox myAgitated;
    CheckBox myAnxious;
    CheckBox myDepressed;
    CheckBox myAlteredLOC;
    CheckBox myImpairedMem;
    CheckBox myPsychHX;
    CheckBox myWNLMental;
    EditText myMentalOther;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mental_layout);

        //connect variables to buttons
        myOriented = findViewById(R.id.orientedText);
        myForgetful = findViewById(R.id.forgetfulBox);
        myConfused = findViewById(R.id.confusedBox);
        myDisoriented = findViewById(R.id.disorientedBox);
        myLethargic = findViewById(R.id.lethargicBox);
        myComatose = findViewById(R.id.comatoseBox);
        myRestless = findViewById(R.id.restlessBox);
        myAgitated = findViewById(R.id.agitatedBox);
        myAnxious = findViewById(R.id.anxiousBox);
        myDepressed = findViewById(R.id.depressedBox);
        myAlteredLOC = findViewById(R.id.alteredBox);
        myImpairedMem = findViewById(R.id.impairedMemBox);
        myPsychHX = findViewById(R.id.psychHXBox);
        myWNLMental = findViewById(R.id.wnlMentalBox);
        myMentalOther = findViewById(R.id.mentalOtherText);

        //load saved state
        Mental mental;
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getStringArray("preExistingMental") != null){
            mental = new Mental();
            mental.massSet(extras.getStringArray("preExistingMental"));
            if (myOriented != null) { myOriented.setText(mental.getOriented()); }
            myForgetful.setChecked(mental.getForgetful());
            myConfused.setChecked(mental.getConfused());
            myDisoriented.setChecked(mental.getDisoriented());
            myLethargic.setChecked(mental.getLethargic());
            myComatose.setChecked(mental.getComatose());
            myRestless.setChecked(mental.getRestless());
            myAgitated.setChecked(mental.getAgitated());
            myAnxious.setChecked(mental.getAnxious());
            myDepressed.setChecked(mental.getDepressed());
            myAlteredLOC.setChecked(mental.getAlteredLOC());
            myImpairedMem.setChecked(mental.getImpairedMem());
            myPsychHX.setChecked(mental.getPsychHX());
            myWNLMental.setChecked(mental.getWnl());
            if (myMentalOther != null) { myMentalOther.setText(mental.getOtherText());}
        }
    }

    public void submitMental(View v){
        String[] returnData = new String[15];
        returnData[0] = myOriented.getText().toString();
        returnData[1] = myForgetful.isChecked() ? "1" : "0";
        returnData[2] = myConfused.isChecked() ? "1" : "0";
        returnData[3] = myDisoriented.isChecked() ? "1" : "0";
        returnData[4] = myLethargic.isChecked() ? "1" : "0";
        returnData[5] = myComatose.isChecked() ? "1" : "0";
        returnData[6] = myRestless.isChecked() ? "1" : "0";
        returnData[7] = myAgitated.isChecked() ? "1" : "0";
        returnData[8] = myAnxious.isChecked() ? "1" : "0";
        returnData[9] = myDepressed.isChecked() ? "1" : "0";
        returnData[10] = myAlteredLOC.isChecked() ? "1" : "0";
        returnData[11] = myImpairedMem.isChecked() ? "1" : "0";
        returnData[12] = myPsychHX.isChecked() ? "1" : "0";
        returnData[13] = myWNLMental.isChecked() ? "1" : "0";
        returnData[14] = myMentalOther.getText().toString();

        Intent returnIntent = new Intent();
        returnIntent.putExtra("returnDataMental", returnData);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
