package com.MedX.hands_on.ui.PatientInfo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.MedX.hands_on.R;

import java.io.Serializable;

public class PatientSearchActivity extends AppCompatActivity {//mergefix commment?

    //TODO: REBUILD
    EditText patientName;
    String searchName;


    PatientInfo patient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_search);


    }

    public void search(View v){
        //TODO: search implementation

        //bind EditText box to patientName to retrieve name to use for database search.
        patientName = findViewById(R.id.patientNameSearchText);
        searchName = patientName.getText().toString();

        //TODO: pass searchName to the database, split however is needed.
        //Could actually perform query on PatientSearchResult? not sure how it needs to happen.
        //Currently it always goes to the same page when the button is clicked. Query database appropriately once connected.
        Intent intent = new Intent(this, PatientSearchResultsActivity.class);
        startActivity(intent); //make startActivityForResult to check success?
    }

    public void endPatientSearch(View v){
        //anything else to do here besides end the activity?
        finish();
    }

}
