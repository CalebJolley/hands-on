package com.MedX.hands_on.ui.PatientInfo;

import java.io.Serializable;

public class Neurological implements Serializable {

    //variable dec
    private boolean headache;
    private boolean syncpe;
    private boolean vertigo;
    private boolean equalGrasp;
    private boolean unEqualGrasp;
    private boolean movement;
    private boolean equalPupils;
    private boolean unEqualPupils;
    private boolean handTremors;
    private boolean aphasia;
    private boolean dysphasia;
    private boolean speechImpair;
    private boolean hearingImpair;
    private boolean visualImpair;
    private boolean wnl;
    private String otherText;

    public void Neurological(){
        headache = false;
        syncpe = false;
        vertigo = false;
        equalGrasp = false;
        unEqualGrasp = false;
        movement = false;
        equalPupils = false;
        unEqualPupils = false;
        handTremors = false;
        aphasia = false;
        dysphasia = false;
        speechImpair = false;
        hearingImpair = false;
        visualImpair = false;
        wnl = false;
        otherText = "";
    }

    public void massSet(String[] data){
        if (data[0].equals("1")) headache = true; else headache = false;
        if (data[1].equals("1")) syncpe = true; else syncpe = false;
        if (data[2].equals("1")) vertigo = true; else vertigo = false;
        if (data[3].equals("1")) equalGrasp= true; else equalGrasp = false;
        if (data[4].equals("1")) unEqualGrasp = true; else unEqualGrasp = false;
        if (data[5].equals("1")) movement = true; else movement = false;
        if (data[6].equals("1")) equalPupils = true; else equalPupils = false;
        if (data[7].equals("1")) unEqualPupils = true; else unEqualPupils = false;
        if (data[8].equals("1")) handTremors = true; else handTremors = false;
        if (data[9].equals("1")) aphasia = true; else aphasia = false;
        if (data[10].equals("1")) dysphasia = true; else dysphasia = false;
        if (data[11].equals("1")) speechImpair = true; else speechImpair = false;
        if (data[12].equals("1")) hearingImpair = true; else hearingImpair = false;
        if (data[13].equals("1")) visualImpair = true; else visualImpair = false;
        if (data[14].equals("1")) wnl = true; else wnl = false;
        if (data[15] == null || data[15].isEmpty()){ otherText = ""; } else { otherText = data[15]; }
    }

    public String[] toStringArray(){

        String[] data = new String[16];

        data[0] = headache ? "1" : "0";
        data[1] = syncpe ? "1" : "0";
        data[2] = vertigo ? "1" : "0";
        data[3] = equalGrasp ? "1" : "0";
        data[4] = unEqualGrasp ? "1" : "0";
        data[5] = movement ? "1" : "0";
        data[6] = equalPupils ? "1" : "0";
        data[7] = unEqualPupils ? "1" : "0";
        data[8] = handTremors ? "1" : "0";
        data[9] = aphasia ? "1" : "0";
        data[10] = dysphasia ? "1" : "0";
        data[11] = speechImpair ? "1" : "0";
        data[12] = hearingImpair ? "1" : "0";
        data[13] = visualImpair ? "1" : "0";
        data[14] = wnl ? "1" : "0";
        if (otherText == null || otherText.isEmpty()){ data[15] = ""; } else { data[15] = otherText; }

        return data;
    }


    //getters
    public boolean getHeadache() {
        return headache;
    }
    public boolean getSyncpe() {
        return syncpe;
    }
    public boolean getVertigo() {
        return vertigo;
    }
    public boolean getEqualGrasp() {
        return equalGrasp;
    }
    public boolean getUnEqualGrasp() {
        return unEqualGrasp;
    }
    public boolean getMovement() {
        return movement;
    }
    public boolean getEqualPupils() {
        return equalPupils;
    }
    public boolean getUnEqualPupils() {
        return unEqualPupils;
    }
    public boolean getHandTremors() {
        return handTremors;
    }
    public boolean getAphasia() {
        return aphasia;
    }
    public boolean getDysphasia() {
        return dysphasia;
    }
    public boolean getSpeechImpair() {
        return speechImpair;
    }
    public boolean getHearingImpair() {
        return hearingImpair;
    }
    public boolean getVisualImpair() {
        return visualImpair;
    }
    public boolean getWnl() {
        return wnl;
    }
    public String getOtherText() {
        return otherText;
    }

    //setters
    public void setHeadache(boolean headache) {
        this.headache = headache;
    }
    public void setSyncpe(boolean syncpe) {
        this.syncpe = syncpe;
    }
    public void setVertigo(boolean vertigo) {
        this.vertigo = vertigo;
    }
    public void setEqualGrasp(boolean equalGrasp) {
        this.equalGrasp = equalGrasp;
    }
    public void setUnEqualGrasp(boolean unEqualGrasp) {
        this.unEqualGrasp = unEqualGrasp;
    }
    public void setMovement(boolean movement) {
        this.movement = movement;
    }
    public void setEqualPupils(boolean equalPupils) {
        this.equalPupils = equalPupils;
    }
    public void setUnEqualPupils(boolean unEqualPupils) {
        this.unEqualPupils = unEqualPupils;
    }
    public void setHandTremors(boolean handTremors) {
        this.handTremors = handTremors;
    }
    public void setAphasia(boolean aphasia) {
        this.aphasia = aphasia;
    }
    public void setDysphasia(boolean dysphasia) {
        this.dysphasia = dysphasia;
    }
    public void setSpeechImpair(boolean speechImpair) {
        this.speechImpair = speechImpair;
    }
    public void setHearingImpair(boolean hearingImpair) {
        this.hearingImpair = hearingImpair;
    }
    public void setVisualImpair(boolean visualImpair) {
        this.visualImpair = visualImpair;
    }
    public void setWnl(boolean wnl) {
        this.wnl = wnl;
    }
    public void setOtherText(String otherText) {
        this.otherText = otherText;
    }

}
