package com.MedX.hands_on.ui.PatientInfo;

public class Mental {

    //variable dec
    private String oriented;
    private boolean forgetful;
    private boolean confused;
    private boolean disoriented;
    private boolean lethargic;
    private boolean comatose;
    private boolean restless;
    private boolean agitated;
    private boolean anxious;
    private boolean depressed;
    private boolean alteredLOC;
    private boolean impairedMem;
    private boolean psychHX;
    private boolean wnl;
    private String otherText;

    public void Mental(){
        oriented = "";
        forgetful = false;
        confused = false;
        disoriented = false;
        lethargic = false;
        comatose = false;
        restless = false;
        agitated = false;
        anxious = false;
        depressed = false;
        alteredLOC = false;
        impairedMem = false;
        psychHX = false;
        wnl = false;
        otherText = "";
    }

    public void massSet(String[] data){
        if (data[0] == null || data[0].isEmpty()){ oriented = ""; } else { oriented = data[0]; }
        if (data[1].equals("1")) forgetful = true; else forgetful = false;
        if (data[2].equals("1")) confused = true; else confused = false;
        if (data[3].equals("1")) disoriented= true; else disoriented = false;
        if (data[4].equals("1")) lethargic = true; else lethargic = false;
        if (data[5].equals("1")) comatose = true; else comatose = false;
        if (data[6].equals("1")) restless = true; else restless = false;
        if (data[7].equals("1")) agitated = true; else agitated = false;
        if (data[8].equals("1")) anxious = true; else anxious = false;
        if (data[9].equals("1")) depressed = true; else depressed = false;
        if (data[10].equals("1")) alteredLOC = true; else alteredLOC = false;
        if (data[11].equals("1")) impairedMem = true; else impairedMem = false;
        if (data[12].equals("1")) psychHX = true; else psychHX = false;
        if (data[13].equals("1")) wnl = true; else wnl = false;
        if(data[14] == null || data[14].isEmpty()){ otherText = ""; } else { otherText = data[14]; }
    }

    public String[] toStringArray(){

        String[] data = new String[15];

        if(oriented == null || oriented.isEmpty()){ data[0] = ""; } else { data[0] = oriented; }
        data[1] = forgetful ? "1" : "0";
        data[2] = confused ? "1" : "0";
        data[3] = disoriented ? "1" : "0";
        data[4] = lethargic ? "1" : "0";
        data[5] = comatose ? "1" : "0";
        data[6] = restless ? "1" : "0";
        data[7] = agitated ? "1" : "0";
        data[8] = anxious ? "1" : "0";
        data[9] = depressed ? "1" : "0";
        data[10] = alteredLOC ? "1" : "0";
        data[11] = impairedMem ? "1" : "0";
        data[12] = psychHX ? "1" : "0";
        data[13] = wnl ? "1" : "0";
        if(otherText == null || otherText.isEmpty()){ data[14] = ""; } else { data[14] = otherText; }

        return data;
    }


    //getters
    public String getOriented() {
        return oriented;
    }
    public boolean getForgetful() {
        return forgetful;
    }
    public boolean getConfused() {
        return confused;
    }
    public boolean getDisoriented() {
        return disoriented;
    }
    public boolean getLethargic() {
        return lethargic;
    }
    public boolean getComatose() {
        return comatose;
    }
    public boolean getRestless() {
        return restless;
    }
    public boolean getAgitated() {
        return agitated;
    }
    public boolean getAnxious() {
        return anxious;
    }
    public boolean getDepressed() {
        return depressed;
    }
    public boolean getAlteredLOC() {
        return alteredLOC;
    }
    public boolean getImpairedMem() {
        return impairedMem;
    }
    public boolean getPsychHX() {
        return psychHX;
    }
    public boolean getWnl() {
        return wnl;
    }
    public String getOtherText() {
        return otherText;
    }

    //setters
    public void setOriented(String oriented) {
        this.oriented = oriented;
    }
    public void setForgetful(boolean forgetful) {
        this.forgetful = forgetful;
    }
    public void setConfused(boolean confused) {
        this.confused = confused;
    }
    public void setDisoriented(boolean disoriented) {
        this.disoriented = disoriented;
    }
    public void setLethargic(boolean lethargic) {
        this.lethargic = lethargic;
    }
    public void setComatose(boolean comatose) {
        this.comatose = comatose;
    }
    public void setRestless(boolean restless) {
        this.restless = restless;
    }
    public void setAgitated(boolean agitated) {
        this.agitated = agitated;
    }
    public void setAnxious(boolean anxious) {
        this.anxious = anxious;
    }
    public void setDepressed(boolean depressed) {
        this.depressed = depressed;
    }
    public void setAlteredLOC(boolean alteredLOC) {
        this.alteredLOC = alteredLOC;
    }
    public void setImpairedMem(boolean impairedMem) {
        this.impairedMem = impairedMem;
    }
    public void setPsychHX(boolean psychHX) {
        this.psychHX = psychHX;
    }
    public void setWnl(boolean wnl) {
        this.wnl = wnl;
    }
    public void setOtherText(String otherText) {
        this.otherText = otherText;
    }


}
