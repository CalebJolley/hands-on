package com.MedX.hands_on.ui.PatientInfo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.MedX.hands_on.R;

import androidx.appcompat.app.AppCompatActivity;

public class PulmonaryActivity extends AppCompatActivity {

    private static final String TAG = "PulmonaryActivity";
    //variable dec
    CheckBox myLungs;
    CheckBox mySobDizzy;
    CheckBox myCough;
    CheckBox mySputum;
    CheckBox myOxygen;
    CheckBox myWNL;
    EditText myPulmonaryOther;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pulmonary_layout);

        //connect variables to buttons
        myLungs = findViewById(R.id.lungsBox);
        mySobDizzy = findViewById(R.id.sobDizzyBox);
        myCough = findViewById(R.id.coughBox);
        mySputum = findViewById(R.id.sputumBox);
        myOxygen = findViewById(R.id.oxygenBox);
        myWNL = findViewById(R.id.wnlPulBox);
        myPulmonaryOther = findViewById(R.id.pulmonaryOtherText);

        //load saved state
        Pulmonary pulmonary;
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getStringArray("preExistingPulmonary") != null){
            pulmonary = new Pulmonary();
            pulmonary.massSet(extras.getStringArray("preExistingPulmonary"));
            myLungs.setChecked(pulmonary.getLungs());
            mySobDizzy.setChecked(pulmonary.getSobDizzy());
            myCough.setChecked(pulmonary.getCough());
            mySputum.setChecked(pulmonary.getSputum());
            myOxygen.setChecked(pulmonary.getOxygen());
            myWNL.setChecked(pulmonary.getWnlPul());
            if(myPulmonaryOther != null){ myPulmonaryOther.setText(pulmonary.getOtherText()); }
            Log.d(TAG, "onCreate: Pulmonary created");
        }
        else {
            Log.d(TAG, "onCreate: Pulmonary extras was null");
        }

    }

    public void submitPulmonary(View v){
        String returnData[] = new String[7];
        returnData[0] = myLungs.isChecked() ? "1" : "0";
        returnData[1] = mySobDizzy.isChecked() ? "1" : "0";
        returnData[2] = myCough.isChecked() ? "1" : "0";
        returnData[3] = mySputum.isChecked() ? "1" : "0";
        returnData[4] = myOxygen.isChecked() ? "1" : "0";
        returnData[5] = myWNL.isChecked() ? "1" : "0";
        returnData[6] = myPulmonaryOther.getText().toString();

        Intent returnIntent = new Intent();
        returnIntent.putExtra("returnDataPulmonary", returnData);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

}
