package com.MedX.hands_on.ui.PatientInfo;

import android.util.Log;

import java.io.Serializable;

public class DrugAdministration implements Serializable {

    private static final String TAG = "Drug Administration";
    //variables
    private String name;
    private String ID;
    private String strength;
    private String date;
    private String dosage;
    private String route;
    private String interval;
    private String physician;
    private String specialInstructions;
    private String otherNotes;
    private String adminDate;
    private String adminTime;

    public void DrugAdministration(){
        //default to false/empty
        name = "";
        ID = "";
        strength = "";
        date = "";
        dosage = "";
        route = "";
        interval = "";
        physician = "";
        specialInstructions = "";
        otherNotes = "";
        adminDate = "";
        adminTime = "";
    }

    //function used to mass set everything to that of input string array
    public void massSet(String[] data){
        if (data[0] == null || data[0].isEmpty()){ name = ""; } else { name = data[0]; }
        if (data[1] == null || data[2].isEmpty()){ ID = ""; } else { ID = data[1]; }
        if (data[2] == null || data[2].isEmpty()){ strength = ""; } else { strength = data[2]; }
        if (data[3] == null || data[3].isEmpty()){ date = ""; } else { date = data[3]; }
        if (data[4] == null || data[4].isEmpty()){ dosage = ""; } else { dosage = data[4]; }
        if (data[5] == null || data[5].isEmpty()){ route = ""; } else { route = data[5]; }
        if (data[6] == null || data[6].isEmpty()){ interval = ""; } else { interval = data[6]; }
        if (data[7] == null || data[7].isEmpty()){ physician = ""; } else { physician = data[7]; }
        if (data[8] == null || data[8].isEmpty()){ specialInstructions = ""; } else { specialInstructions = data[8]; }
        if (data[9] == null || data[9].isEmpty()){ otherNotes = ""; } else { otherNotes = data[9]; }
        if (data[10] == null || data[10].isEmpty()){ adminDate = ""; } else { adminDate = data[10]; }
        if (data[11] == null || data[11].isEmpty()){ adminTime = ""; } else { adminTime = data[11]; }
        Log.d(TAG, "massSet: Mass set completed successfully");
    }

    public String[] toStringArray(){

        String[] data = new String[12];

        if (name == null || name.isEmpty()){ data[0] = ""; } else { data[0] = name; }
        if (ID == null || ID.isEmpty()){ data[1] = ""; } else { data[1] = ID; }
        if (strength == null || strength.isEmpty()){ data[2] = ""; } else { data[2] = strength; }
        if (date == null || date.isEmpty()){ data[3] = ""; } else { data[3] = date; }
        if (dosage == null || dosage.isEmpty()){ data[4] = ""; } else { data[4] = dosage; }
        if (route == null || route.isEmpty()){ data[5] = ""; } else { data[5] = route; }
        if (interval == null || interval.isEmpty()){ data[6] = ""; } else { data[6] = interval; }
        if (physician == null || physician.isEmpty()){ data[7] = ""; } else { data[7] = physician; }
        if (specialInstructions == null || specialInstructions.isEmpty()){ data[8] = ""; } else { data[8] = specialInstructions; }
        if (otherNotes == null || otherNotes.isEmpty()){ data[9] = ""; } else { data[9] = otherNotes; }//1, 0, 1, 1, 0, ..., "other text"
        if (adminDate == null || adminDate.isEmpty()){ data[10] = ""; } else { data[10] = adminDate; }
        if (adminTime == null || adminTime.isEmpty()){ data[11] = ""; } else { data[11] = adminTime; }

        return data;
    }

    //getters
    public String getName(){
        return name;
    }
    public String getID(){
        return ID;
    }
    public String getStrength(){
        return strength;
    }
    public String getDate(){
        return date;
    }
    public String getDosage(){
        return dosage;
    }
    public String getRoute(){
        return route;
    }
    public String getInterval(){
        return interval;
    }
    public String getPhysician(){
        return physician;
    }
    public String getSpecialInstructions(){
        return specialInstructions;
    }
    public String getOtherNotes(){
        return otherNotes;
    }
    public String getAdminDate(){
        return adminDate;
    }
    public String getAdminTime(){
        return adminTime;
    }

    //setters
    public void setName(String b){
        name = b;
    }
    public void setID(String b){
        ID = b;
    }
    public void setStrength(String b){
        strength = b;
    }
    public void setDate(String b){
        date = b;
    }
    public void setDosage(String b){
        dosage = b;
    }
    public void setRoute(String b){
        route = b;
    }
    public void setInterval(String b){
        interval = b;
    }
    public void setPhysician(String b){
        physician = b;
    }
    public void setSpecialInstructions(String b){
        specialInstructions = b;
    }
    public void setOtherNotes(String text){
        otherNotes = text;
    }
    public void setAdminDate(String text){
        adminDate = text;
    }
    public void setAdminTime(String text){
        adminTime = text;
    }
}
