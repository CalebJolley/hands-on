package com.MedX.hands_on.ui.PatientInfo;

import java.io.Serializable;

public class Integumentary implements Serializable {

    //variable dec
    private boolean warm;
    private boolean dry;
    private boolean cool;
    private boolean chills;
    private boolean intact;
    private boolean wound;
    private boolean ulcer;
    private boolean incision;
    private boolean rash;
    private boolean itching;
    private boolean turgor;
    private boolean wnl;
    private String otherText;

    public void Integumentary(){
        warm = false;
        dry = false;
        cool = false;
        chills = false;
        intact = false;
        wound = false;
        ulcer = false;
        incision = false;
        rash = false;
        itching = false;
        turgor = false;
        wnl = false;
        otherText = "";
    }

    public void massSet(String[] data){
        if (data[0].equals("1")) warm = true; else warm = false;
        if (data[1].equals("1")) dry = true; else dry = false;
        if (data[2].equals("1")) cool = true; else cool = false;
        if (data[3].equals("1")) chills= true; else chills = false;
        if (data[4].equals("1")) intact = true; else intact = false;
        if (data[5].equals("1")) wound = true; else wound = false;
        if (data[6].equals("1")) ulcer = true; else ulcer = false;
        if (data[7].equals("1")) incision = true; else incision = false;
        if (data[8].equals("1")) rash = true; else rash = false;
        if (data[9].equals("1")) itching = true; else itching = false;
        if (data[10].equals("1")) turgor = true; else turgor = false;
        if (data[11].equals("1")) wnl = true; else wnl = false;
        if(data[12] == null || data[12].isEmpty()){ otherText = ""; } else { otherText = data[11]; }
    }

    public String[] toStringArray(){

        String[] data = new String[13];

        data[0] = warm ? "1" : "0";
        data[1] = dry ? "1" : "0";
        data[2] = cool ? "1" : "0";
        data[3] = chills ? "1" : "0";
        data[4] = intact ? "1" : "0";
        data[5] = wound ? "1" : "0";
        data[6] = ulcer ? "1" : "0";
        data[7] = incision ? "1" : "0";
        data[8] = rash ? "1" : "0";
        data[9] = itching ? "1" : "0";
        data[10] = turgor ? "1" : "0";
        data[11] = wnl ? "1" : "0";
        if(otherText == null || otherText.isEmpty()){ data[12] = ""; } else { data[12] = otherText; }

        return data;
    }

    //getters
    public boolean getWarm(){
        return warm;
    }
    public boolean getDry(){
        return dry;
    }
    public boolean getCool(){
        return cool;
    }
    public boolean getChills(){
        return chills;
    }
    public boolean getIntact(){
        return intact;
    }
    public boolean getWound(){
        return wound;
    }
    public boolean getUlcer(){
        return ulcer;
    }
    public boolean getIncision(){
        return incision;
    }
    public boolean getRash(){
        return rash;
    }
    public boolean getItching(){
        return itching;
    }
    public boolean getTurgor(){
        return turgor;
    }
    public boolean getWNL(){
        return wnl;
    }
    public String getOtherText(){
        return otherText;
    }

    //setters
    public void setWarm(boolean b){
        warm = b;
    }
    public void setDry(boolean b){
        dry = b;
    }
    public void setCool(boolean b){
        cool = b;
    }
    public void setChills(boolean b){
        chills = b;
    }
    public void setIntact(boolean b){
        intact = b;
    }
    public void setWound(boolean b){
        wound = b;
    }
    public void setUlcer(boolean b){
        ulcer = b;
    }
    public void setIncision(boolean b){
        incision = b;
    }
    public void setRash(boolean b){
        rash = b;
    }
    public void setItching(boolean b){
        itching = b;
    }
    public void setTurgor(boolean b){
        turgor = b;
    }
    public void setWnl(boolean b){
        wnl = b;
    }
    public void setOtherText(String s){
        otherText = s;
    }

}
