package com.MedX.hands_on.ui.PatientInfo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.MedX.hands_on.R;

import androidx.appcompat.app.AppCompatActivity;

public class GastroIntestinalActivity extends AppCompatActivity {


    //variable dec
    CheckBox myBowelSounds;
    CheckBox myAbdomenSoft;
    CheckBox myAbdomenTender;
    CheckBox myDistended;
    CheckBox myNausea;
    CheckBox myVomiting;
    CheckBox myNPO;
    CheckBox myDiarrhea;
    CheckBox myConstipation;
    CheckBox myIncontinenceGastro;
    CheckBox myOstomy;
    CheckBox myPEG;
    CheckBox myFeeding;
    CheckBox myFlushing;
    EditText myLastBM;
    CheckBox myWNLGastro;
    EditText myGastroOther;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gastro_layout);

        //connect variables to buttons
        myBowelSounds = findViewById(R.id.bowelSoundsBox);
        myAbdomenSoft = findViewById(R.id.abdominSoftBox);
        myAbdomenTender = findViewById(R.id.abdomenTenderBox);
        myDistended = findViewById(R.id.distendedBox);
        myNausea = findViewById(R.id.nauseaGastroBox);
        myVomiting = findViewById(R.id.vomitingBox);
        myNPO = findViewById(R.id.npoBox);
        myDiarrhea = findViewById(R.id.diarrheaBox);
        myConstipation = findViewById(R.id.constipationBox);
        myIncontinenceGastro = findViewById(R.id.incontinenceGastroBox);
        myOstomy = findViewById(R.id.ostomyBox);
        myPEG = findViewById(R.id.pegBox);
        myFeeding = findViewById(R.id.feedingBox);
        myFlushing = findViewById(R.id.flushingBox);
        myLastBM = findViewById(R.id.lastBMText);
        myWNLGastro = findViewById(R.id.wnlGastroBox);
        myGastroOther = findViewById(R.id.gastroOtherText);

        //load saved state
        GastroIntestinal gastroIntestinal;
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getStringArray("preExistingGastro") != null){
            gastroIntestinal = new GastroIntestinal();
            gastroIntestinal.massSet(extras.getStringArray("preExistingGastro"));
            myBowelSounds.setChecked((gastroIntestinal.getBowelSounds()));
            myAbdomenSoft.setChecked(gastroIntestinal.getAbdomenSoft());
            myAbdomenTender.setChecked(gastroIntestinal.getAbdomenTender());
            myDistended.setChecked(gastroIntestinal.getDistended());
            myNausea.setChecked(gastroIntestinal.getNausea());
            myVomiting.setChecked(gastroIntestinal.getVomiting());
            myNPO.setChecked(gastroIntestinal.getNpo());
            myDiarrhea.setChecked(gastroIntestinal.getDiarrhea());
            myConstipation.setChecked(gastroIntestinal.getConstipation());
            myIncontinenceGastro.setChecked(gastroIntestinal.getIncontinence());
            myOstomy.setChecked(gastroIntestinal.getOstomy());
            myPEG.setChecked(gastroIntestinal.getPeg());
            myFeeding.setChecked(gastroIntestinal.getFeeding());
            myFlushing.setChecked(gastroIntestinal.getFlushing());
            if(myLastBM != null){ myLastBM.setText(gastroIntestinal.getLastBM()); }
            myWNLGastro.setChecked(gastroIntestinal.getWnl());
            if(myGastroOther != null){ myGastroOther.setText(gastroIntestinal.getOtherText()); }
        }
    }

    public void submitGastro(View v){
        String[] returnData = new String[17];
        returnData[0] = myBowelSounds.isChecked() ? "1" : "0";
        returnData[1] = myAbdomenSoft.isChecked() ? "1" : "0";
        returnData[2] = myAbdomenTender.isChecked() ? "1" : "0";
        returnData[3] = myDistended.isChecked() ? "1" : "0";
        returnData[4] = myNausea.isChecked() ? "1" : "0";
        returnData[5] = myVomiting.isChecked() ? "1" : "0";
        returnData[6] = myNPO.isChecked() ? "1" : "0";
        returnData[7] = myDiarrhea.isChecked() ? "1" : "0";
        returnData[8] = myConstipation.isChecked() ? "1" : "0";
        returnData[9] = myIncontinenceGastro.isChecked() ? "1" : "0";
        returnData[10] = myOstomy.isChecked() ? "1" : "0";
        returnData[11] = myPEG.isChecked() ? "1" : "0";
        returnData[12] = myFeeding.isChecked() ? "1" : "0";
        returnData[13] = myFlushing.isChecked() ? "1" : "0";
        returnData[14] = myLastBM.getText().toString();
        returnData[15] = myWNLGastro.isChecked() ? "1" : "0";
        returnData[16] = myGastroOther.getText().toString();

        Intent returnIntent = new Intent();
        returnIntent.putExtra("returnDataGastro", returnData);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
