package com.MedX.hands_on.ui.PatientInfo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.MedX.hands_on.R;

import java.io.Serializable;
import java.util.ArrayList;

public class PatientInfoFieldsActivity extends AppCompatActivity {

    //textbox declaration
    EditText patientName;
    EditText patientEmail;
    EditText patientPhoneNum;
    EditText patientAddress;
    EditText patientBirthDate;
    EditText vistType;



    private static final String TAG = "PatientInfoFields";
    //activity codes
    int launchCardioActivity = 1;
    int launchPulmonaryActivity = 2;
    int launchIntegumentaryActivity = 3;
    int launchMSActivity = 4;
    int launchGastroActivity = 5;
    int launchGenitourinaryActivity = 6;
    int launchNeuroActivity = 7;
    int launchMentalActivity = 8;
    int launchPainActivity = 9;
    int launchInterventionActivity = 10;
    int launchTechniqueActivity = 11;
    int launchInfusionActivity = 12;
    int launchDrugActivity = 13;

    //object definition
    //holds all data from fields.
    Cardiovascular cardiovascular;
    Pulmonary pulmonary;
    Integumentary integumentary;
    MusculoSkeletal musculoSkeletal;
    GastroIntestinal gastroIntestinal;
    Genitourinary genitourinary;
    Neurological neurological;
    Mental mental;
    Pain pain;
    Intervention intervention;
    Technique technique;
    Infusion infusion;
    PatientInfo patient;
    ArrayList drugs;


    @Override
    protected void onCreate(Bundle savedInstanceState) { //mergefix commment?
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_info_fields);


        //loading from the database will go here. This will go in an if statement for new forms
        //TODO: loading


        cardiovascular = new Cardiovascular();
        pulmonary = new Pulmonary();
        integumentary = new Integumentary();
        musculoSkeletal = new MusculoSkeletal();
        gastroIntestinal = new GastroIntestinal();
        genitourinary = new Genitourinary();
        neurological = new Neurological();
        mental = new Mental();
        pain = new Pain();
        intervention = new Intervention();
        technique = new Technique();
        infusion = new Infusion();
        patient = new PatientInfo();
        drugs = new ArrayList();//stores date internally, not accessed by PatientInfoFields.
    } //end onCreate

    //removed else that creates a new instance of the class if the object is NULL.
    // //This is to help troubleshooting loading problems once the database is connected.
    //buttons will do nothing if it loads improperly.
    //TODO: make buttons log they were null before returning to mainmenu.

    public void cardioContents(View v){
        if(cardiovascular != null) {
            Intent intent = new Intent(this, CardiovascularActivity.class);
            intent.putExtra("preExistingCardio", cardiovascular.toStringArray());
            startActivityForResult(intent, launchCardioActivity);
        }
    }

    public void pulmonaryContents(View v){
        if(pulmonary != null) {
            Intent intent = new Intent(this, PulmonaryActivity.class);
            intent.putExtra("preExistingPulmonary", pulmonary.toStringArray());
            startActivityForResult(intent, launchPulmonaryActivity);
        }
    }

    public void integumentaryContents(View v){
        if(integumentary != null) {
            Intent intent = new Intent(this, IntegumentaryActivity.class);
            intent.putExtra("preExistingInteg", integumentary.toStringArray());
            startActivityForResult(intent, launchIntegumentaryActivity);
        }
    }

    public void musculoskeletalContents(View v){
        if(musculoSkeletal != null) {
            Intent intent = new Intent(this, MusculoSkeletalActivity.class);
            intent.putExtra("preExistingMusc", musculoSkeletal.toStringArray());
            startActivityForResult(intent, launchMSActivity);
        }
    }

    public void gastrointestinalContents(View v){
        if(gastroIntestinal != null){
            Intent intent = new Intent(this, GastroIntestinalActivity.class);
            intent.putExtra("preExistingGastro", gastroIntestinal.toStringArray());
            startActivityForResult(intent, launchGastroActivity);
        }
    }

    public void genitourinaryContents(View v){
        if(genitourinary != null) {
            Intent intent = new Intent(this, GenitourinaryActivity.class);
            intent.putExtra("preExistingGenitour", genitourinary.toStringArray());
            startActivityForResult(intent, launchGenitourinaryActivity);
        }
    }

    public void neurologicalContents(View v){

        if(neurological != null) {
            Intent intent = new Intent(this, NeurologicalActivity.class);
            intent.putExtra("preExistingNeuro", neurological.toStringArray());
            startActivityForResult(intent, launchNeuroActivity);
        }
    }

    public void mentalContents(View v){
        if (mental != null) {
            Intent intent = new Intent(this, MentalActivity.class);
            intent.putExtra("preExistingMental", mental.toStringArray());
            startActivityForResult(intent, launchMentalActivity);
        }
    }

    public void painContents(View v){
        if (pain != null) {
            Intent intent = new Intent(this, PainActivity.class);
            intent.putExtra("preExistingPain", pain.toStringArray());
            startActivityForResult(intent, launchPainActivity);
        }
    }

    public void interventionsContent(View v){
        if(intervention != null) {
            Intent intent = new Intent(this, InterventionActivity.class);
            intent.putExtra("preExistingIntervention", intervention.toStringArray());
            startActivityForResult(intent, launchInterventionActivity);
        }
    }

    public void techniquesUsedContent(View v){
        if (technique != null) {
            Intent intent = new Intent(this, TechniqueActivity.class);
            intent.putExtra("preExistingTechnique", technique.toStringArray());
            startActivityForResult(intent, launchTechniqueActivity);
        }
    }

    public void infusionContent(View v){
        if (infusion != null) {
            Intent intent = new Intent(this, InfusionActivity.class);
            intent.putExtra("preExistingInfusion", infusion.toStringArray());
            startActivityForResult(intent, launchInfusionActivity);
        }
    }

    public void drugContent(View v){
        Intent intent = new Intent(this, DrugAdministrationActivity.class);
        intent.putExtra("numDrugs", Integer.toString(drugs.size()));
        if (drugs.size() != 0) {
            for (int i = 0; i < drugs.size(); i++) {
                intent.putExtra(("preExistingDrug" + Integer.toString(i)), ((DrugAdministration) drugs.get(i)).toStringArray());
            }
        }
            startActivityForResult(intent, launchDrugActivity);
    }

    public void submitPatientInfo(View v){
        //This is where we will send stuff to the database.
        //You'll also need to grab the patient info from the text boxes.

        //TODO: once all forms are finished, check to make sure Patient has all of them and that they are properly saved.
        //save data from off-shoot pages
        patient.setCardiovascular(cardiovascular);
        patient.setPulmonary(pulmonary);
        patient.setIntegumentary(integumentary);
        patient.setMusculoSkeletal(musculoSkeletal);
        patient.setGastroIntestinal(gastroIntestinal);
        patient.setGenitourinary(genitourinary);
        patient.setNeurological(neurological);
        patient.setMental(mental);
        patient.setPain(pain);
        patient.setIntervention(intervention);
        patient.setTechnique(technique);
        patient.setInfusion(infusion);
        patient.setDrugs(drugs);

        //connect textboxes and save information from them.
        patientName = (EditText) findViewById(R.id.patientInfoFieldsNameText);
        patientBirthDate = (EditText) findViewById(R.id.patientInfoFieldsBdayText);
        patientEmail = (EditText) findViewById(R.id.patientInfoFieldsEmailText);
        patientAddress = (EditText) findViewById(R.id.patientInfoFieldsAddress);
        patientPhoneNum = (EditText) findViewById(R.id.patientInfoFieldsPhoneText);
        vistType = findViewById(R.id.visitTypeText);
        if(patientName != null){ patient.setPatientName(patientName.getText().toString()); }
        if(patientAddress != null){ patient.setPatientAddress(patientAddress.getText().toString()); }
        if(patientEmail != null){ patient.setPatientEmail(patientEmail.getText().toString()); }
        if(patientBirthDate != null){ patient.setPatientBirthDate(patientBirthDate.getText().toString()); }
        if(patientPhoneNum != null){ patient.setPatientPhoneNum(patientPhoneNum.getText().toString()); }
        if(vistType != null) {patient.setVisitType(vistType.getText().toString()); }


        //TODO: send data to database and close this form.
        //NOTE: if this form is loaded (aka the primary key (name + date?) already exists), it should update the database instead of attempt to create a new entry.


        //close the activity. All data will be lost and the activity will not be retrievable past this point.
        finish();

    } //end submitPatientInfo

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        //this function takes result data and saves it into objects, to later be packaged and sent to
        //the server as required.

        if (resultCode == RESULT_OK) {
            //Should we make this a switch?
            if (requestCode == Activity.RESULT_CANCELED) {
                //TODO: write code if there's no result.
            }
            else if (requestCode == launchCardioActivity) {
                cardiovascular.massSet(data.getStringArrayExtra("returnDataCardio"));
                Log.d(TAG, "onActivityResult: Cardiovascular updated");
            }
            else if (requestCode == launchPulmonaryActivity){
                pulmonary.massSet(data.getStringArrayExtra("returnDataPulmonary"));
                Log.d(TAG, "onActivityResult: Pulmonary updated");
            }
            else if (requestCode == launchIntegumentaryActivity){
                integumentary.massSet(data.getStringArrayExtra("returnDataInteg"));
                Log.d(TAG, "onActivityResult: Integumentary updated");
            }
            else if (requestCode == launchMSActivity){
                musculoSkeletal.massSet(data.getStringArrayExtra("returnDataMusc"));
                Log.d(TAG, "onActivityResult: MusculoSkeletal updated");
            }
            else if (requestCode == launchGastroActivity){
                gastroIntestinal.massSet(data.getStringArrayExtra("returnDataGastro"));
                Log.d(TAG, "onActivityResult: Gastrointestinal updated");
            }
            else if (requestCode == launchGenitourinaryActivity){
                genitourinary.massSet(data.getStringArrayExtra("returnDataGenitour"));
                Log.d(TAG, "onActivityResult: Genitourinary updated");
            }
            else if (requestCode == launchNeuroActivity){
                neurological.massSet(data.getStringArrayExtra("returnDataNeuro"));
                Log.d(TAG, "onActivityResult: Neurological updated");
            }
            else if (requestCode == launchMentalActivity){
                mental.massSet(data.getStringArrayExtra("returnDataMental"));
                Log.d(TAG, "onActivityResult: Mental updated");
            }
            else if (requestCode == launchPainActivity){
                pain.massSet(data.getStringArrayExtra("returnDataPain"));
                Log.d(TAG, "onActivityResult: Pain updated");
            }
            else if (requestCode == launchInterventionActivity){
                intervention.massSet(data.getStringArrayExtra("returnDataIntervention"));
                Log.d(TAG, "onActivityResult: Intervention updated");
            }
            else if (requestCode == launchTechniqueActivity){
                technique.massSet(data.getStringArrayExtra("returnDataTechnique"));
                Log.d(TAG, "onActivityResult: Technique updated");
            }
            else if (requestCode == launchInfusionActivity){
                infusion.massSet(data.getStringArrayExtra("returnDataInfusion"));
                Log.d(TAG, "onActivityResult: Infusion updated");
            }
            else if (requestCode == launchDrugActivity){
                //drugs.massSet(data.getStringArrayExtra("returnDataDrug"));
                int count = data.getIntExtra("numDrugs", 0);

                if(count > drugs.size()){
                    for(int i = drugs.size(); i < count; i++){
                        drugs.add(i, new DrugAdministration());
                    }
                }

                for(int i = 0; i < count; i++){
                    ((DrugAdministration) drugs.get(i)).massSet(data.getStringArrayExtra(("returnDataDrug" + Integer.toString(i))));
                }

                Log.d(TAG, "onActivityResult: Drugs updated");
            }
        }


    } //end onActivityResult

        //Chris - Create an Activity for each section in the document that I have not already created
        //and set up the data fields. Make sure to create a link button similar to MainMenuActivity
        // but do not worry about layout. I will add more info as I work more
}