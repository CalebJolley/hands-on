package com.MedX.hands_on.ui.PatientInfo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.MedX.hands_on.R;

import androidx.appcompat.app.AppCompatActivity;

public class CardiovascularActivity extends AppCompatActivity {

    private static final String TAG = "Cardio Extras";
    //variable dec
    CheckBox myChestPain;
    CheckBox myRue;
    CheckBox myLue;
    CheckBox myRle;
    CheckBox myLle;
    CheckBox myAbRhythm;
    CheckBox myPulses;
    CheckBox myAntiCoag;
    CheckBox myWnlCardio;
    EditText myCardioOther;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cardiovascular_layout);

        //connect variables to buttons
        myChestPain = findViewById(R.id.chestPainBox);
        myRue = findViewById(R.id.rueBox);
        myLue = findViewById(R.id.lueBox);
        myRle = findViewById(R.id.rleBox);
        myLle = findViewById(R.id.lleBox);
        myAbRhythm = findViewById(R.id.abRhythmBox);
        myPulses = findViewById(R.id.pulseBox);
        myAntiCoag = findViewById(R.id.antiCoagBox);
        myWnlCardio = findViewById(R.id.wnlCardioBox);
        myCardioOther = findViewById(R.id.cardioOtherText);


        //load saved state.
        Cardiovascular cardiovascular;
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getStringArray("preExistingCardio") != null){
            cardiovascular = new Cardiovascular();
            cardiovascular.massSet(extras.getStringArray("preExistingCardio"));
            myChestPain.setChecked(cardiovascular.getChestPain());
            myRue.setChecked(cardiovascular.getRue());
            myLue.setChecked(cardiovascular.getLue());
            myRle.setChecked(cardiovascular.getRle());
            myLle.setChecked(cardiovascular.getLle());
            myAbRhythm.setChecked(cardiovascular.getAbRhythm());
            myPulses.setChecked(cardiovascular.getPulses());
            myAntiCoag.setChecked(cardiovascular.getAntiCoag());
            myWnlCardio.setChecked(cardiovascular.getWnl());
            if(myCardioOther != null){ myCardioOther.setText(cardiovascular.getOtherText()); }
            Log.d(TAG, "onCreate: Cardiovascular created");
        }
        else {
            Log.d(TAG, "onCreate: Cardio extras was null");
        }




    }

    public void submitCardio(View v) {
        String returnData[] = new String[11];
        returnData[0] = myChestPain.isChecked() ? "1" : "0";
        returnData[1] = myRue.isChecked() ? "1" : "0";
        returnData[2] = myLue.isChecked() ? "1" : "0";
        returnData[3] = myRle.isChecked() ? "1" : "0";
        returnData[4] = myLle.isChecked() ? "1" : "0";
        returnData[5] = myAbRhythm.isChecked() ? "1" : "0";
        returnData[6] = myPulses.isChecked() ? "1" : "0";
        returnData[7] = myAntiCoag.isChecked() ? "1" : "0";
        returnData[8] = myWnlCardio.isChecked() ? "1" : "0";
        returnData[9] = myCardioOther.getText().toString();


        Intent returnIntent = new Intent();
        returnIntent.putExtra("returnDataCardio", returnData);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }


}
