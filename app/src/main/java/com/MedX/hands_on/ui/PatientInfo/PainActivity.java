package com.MedX.hands_on.ui.PatientInfo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.MedX.hands_on.R;

import androidx.appcompat.app.AppCompatActivity;

public class PainActivity extends AppCompatActivity {

    //variable dec
    CheckBox myNoPain;
    CheckBox myNotDailyPain;
    CheckBox myDailyPain;
    CheckBox myConstantPain;
    EditText myPainLevel;
    EditText myPainSite;
    CheckBox myRelievedMedYes;
    CheckBox getMyRelievedMedno;
    EditText myPainOther;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pain_layout);

        //connect variables to buttons
        myNoPain = findViewById(R.id.noPainBox);
        myNotDailyPain = findViewById(R.id.notDailyPainBox);
        myDailyPain = findViewById(R.id.dailyPainBox);
        myConstantPain = findViewById(R.id.constantPainBox);
        myPainLevel = findViewById(R.id.painLevelText);
        myPainSite = findViewById(R.id.painSiteText);
        myRelievedMedYes = findViewById(R.id.relievedMedYesBox);
        getMyRelievedMedno = findViewById(R.id.relievedMedNoBox);
        myPainOther = findViewById(R.id.painOtherText);

        //load saved state
        Pain pain;
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getStringArray("preExistingPain") != null){
            pain = new Pain();
            pain.massSet(extras.getStringArray("preExistingPain"));
            myNoPain.setChecked(pain.getNoPain());
            myNotDailyPain.setChecked(pain.getNotDailyPain());
            myDailyPain.setChecked(pain.getDailyPain());
            myConstantPain.setChecked(pain.getConstantPain());
            if (myPainLevel != null) {myPainLevel.setText(pain.getPainLevel());}
            if (myPainSite != null) {myPainSite.setText(pain.getPainSite());}
            myRelievedMedYes.setChecked(pain.getRelievedMedYes());
            getMyRelievedMedno.setChecked(pain.getRelievedMedNo());
            if(myPainOther != null) {myPainOther.setText(pain.getOtherText());}
        }
    }

    public void submitPain(View v){
        String[] returnData = new String[9];

        returnData[0] = myNoPain.isChecked() ? "1" : "0";
        returnData[1] = myNotDailyPain.isChecked() ? "1" : "0";
        returnData[2] = myDailyPain.isChecked() ? "1" : "0";
        returnData[3] = myConstantPain.isChecked() ? "1" : "0";
        returnData[4] = myPainLevel.getText().toString();
        returnData[5] = myPainSite.getText().toString();
        returnData[6] = myRelievedMedYes.isChecked() ? "1" : "0";
        returnData[7] = getMyRelievedMedno.isChecked() ? "1" : "0";
        returnData[8] = myPainOther.getText().toString();

        Intent returnIntent = new Intent();
        returnIntent.putExtra("returnDataPain", returnData);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }


}
