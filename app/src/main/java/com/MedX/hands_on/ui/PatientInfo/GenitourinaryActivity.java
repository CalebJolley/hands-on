package com.MedX.hands_on.ui.PatientInfo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.MedX.hands_on.R;

import androidx.appcompat.app.AppCompatActivity;

public class GenitourinaryActivity extends AppCompatActivity {

    //variable dec
    CheckBox myBurning;
    CheckBox myDysuria;
    CheckBox myOdor;
    CheckBox myDistention;
    CheckBox myRetention;
    CheckBox myFrequency;
    CheckBox myUrgency;
    CheckBox myIncontinence;
    CheckBox myHesitance;
    CheckBox myItching;
    EditText myColor;
    EditText myCatheter;
    EditText myFR;
    EditText myCC;
    EditText myLastChanged;
    CheckBox myIrrigation;
    CheckBox myWNLGenitour;
    EditText myGenitourOther;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.genitourinary_layout);

        //connect variables to buttons
        myBurning = findViewById(R.id.burningBox);
        myDysuria = findViewById(R.id.dysuriaBox);
        myOdor = findViewById(R.id.odorBox);
        myDistention = findViewById(R.id.distentionBox);
        myRetention = findViewById(R.id.retentionBox);
        myFrequency = findViewById(R.id.frequencyBox);
        myUrgency = findViewById(R.id.urgencyBox);
        myIncontinence = findViewById(R.id.incontinenceGenitourBox);
        myHesitance = findViewById(R.id.hesitanceBox);
        myItching = findViewById(R.id.itchingGenitourBox);
        myColor = findViewById(R.id.colorText);
        myCatheter = findViewById(R.id.catheterText);
        myFR = findViewById(R.id.catheterFRText);
        myCC = findViewById(R.id.catheterCCText);
        myLastChanged = findViewById(R.id.lastChangedText);
        myIrrigation = findViewById(R.id.irrigationGenitourBox);
        myWNLGenitour = findViewById(R.id.wnlGenitourBox);
        myGenitourOther = findViewById(R.id.genitourOtherText);

        //load saved state
        Genitourinary genitourinary;
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getStringArray("preExistingGenitour") != null){
            genitourinary = new Genitourinary();
            genitourinary.massSet(extras.getStringArray("preExistingGenitour"));
            myBurning.setChecked(genitourinary.getBurning());
            myDysuria.setChecked(genitourinary.getDysuria());
            myOdor.setChecked(genitourinary.getOdor());
            myDistention.setChecked(genitourinary.getDistention());
            myRetention.setChecked(genitourinary.getRetention());
            myFrequency.setChecked(genitourinary.getFrequency());
            myUrgency.setChecked(genitourinary.getUrgency());
            myIncontinence.setChecked(genitourinary.getIncontinence());
            myHesitance.setChecked(genitourinary.getHesitance());
            myItching.setChecked(genitourinary.getItching());
            if (myColor != null) { myColor.setText(genitourinary.getColor()); }
            if (myCatheter != null){ myCatheter.setText(genitourinary.getCatheter()); }
            if (myFR != null) { myFR.setText(genitourinary.getFr()); }
            if (myCC != null) { myCC.setText(genitourinary.getCc()); }
            if (myLastChanged != null) { myLastChanged.setText(genitourinary.getLastChanged()); }
            myIrrigation.setChecked(genitourinary.getIrrigation());
            myWNLGenitour.setChecked(genitourinary.getWnl());
            if (myGenitourOther != null) { myGenitourOther.setText(genitourinary.getOtherText()); }
        }
    }

            public void submitGenitour(View v){
                String[] returnData = new String[18];
                returnData[0] = myBurning.isChecked() ? "1" : "0";
                returnData[1] = myDysuria.isChecked() ? "1" : "0";
                returnData[2] = myOdor.isChecked() ? "1" : "0";
                returnData[3] = myDistention.isChecked() ? "1" : "0";
                returnData[4] = myRetention.isChecked() ? "1" : "0";
                returnData[5] = myFrequency.isChecked() ? "1" : "0";
                returnData[6] = myUrgency.isChecked() ? "1" : "0";
                returnData[7] = myIncontinence.isChecked() ? "1" : "0";
                returnData[8] = myHesitance.isChecked() ? "1" : "0";
                returnData[9] = myItching.isChecked() ? "1" : "0";
                returnData[10] = myColor.getText().toString();
                returnData[11] = myCatheter.getText().toString();
                returnData[12] = myFR.getText().toString();
                returnData[13] = myCC.getText().toString();
                returnData[14] = myLastChanged.getText().toString();
                returnData[15] = myIrrigation.isChecked() ? "1" : "0";
                returnData[16] = myWNLGenitour.isChecked() ? "1" : "0";
                returnData[17] = myGenitourOther.getText().toString();

                Intent returnIntent = new Intent();
                returnIntent.putExtra("returnDataGenitour", returnData);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
}
