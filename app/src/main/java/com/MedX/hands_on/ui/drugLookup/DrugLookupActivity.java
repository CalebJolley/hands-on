package com.MedX.hands_on.ui.drugLookup;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import java.net.*;//HttpURLConnection;
import java.io.*;//InputStream;

import com.MedX.hands_on.R;

public class DrugLookupActivity extends AppCompatActivity {

    EditText desiredDrugName;
    EditText desiredUPC;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drug_lookup);

        //textbox + button definitions
        desiredDrugName = findViewById(R.id.desiredDrugName);
        desiredUPC = findViewById(R.id.drugUPC);
    }


    public void findDrug(View v) throws IOException{

        String desiredDrugNameString = desiredDrugName.getText().toString();
        String desiredUPCString = desiredUPC.getText().toString();

        //TODO: send above strings to the database and build the results page.
        //https://rxnav.nlm.nih.gov/REST/Prescribe/rxcui.json?name=lipitor
        //https://rxnav.nlm.nih.gov/REST/Prescribe/drugs?name=cymbalta     might need to use this one
        //https://rxnav.nlm.nih.gov/REST/Prescribe/approximateTerm?term=zocor%2010%20mg&maxEntries=5

        /*URL url = new URL("https://rxnav.nlm.nih.gov/REST/Prescribe/approximateTerm?term="+ desiredDrugNameString + "&maxEntries=5");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String xml = readStream(in);
        }
        finally {
            urlConnection.disconnect();
        }
        //

    }

    private String readStream(InputStream is) {
        try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            int i = is.read();
            while(i != -1) {
                bo.write(i);
                i = is.read();
            }
            return bo.toString();
        } catch (IOException e) {
            return "";
        }*/
    }
}
