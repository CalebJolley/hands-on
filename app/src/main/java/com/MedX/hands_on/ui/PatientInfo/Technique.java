package com.MedX.hands_on.ui.PatientInfo;

public class Technique {

    //variable dec
    private boolean precautions;
    private boolean asepticTech;
    private boolean sharpObjDisp;
    private boolean wasteDisp;
    private boolean glucometer;
    private String glucometerCalib;
    private String otherText;

    public void massSet(String[] data){
        if (data[0].equals("1")) precautions = true; else precautions = false;
        if (data[1].equals("1")) asepticTech = true; else asepticTech = false;
        if (data[2].equals("1"))  sharpObjDisp= true; else sharpObjDisp = false;
        if (data[3].equals("1")) wasteDisp= true; else wasteDisp = false;
        if (data[4].equals("1")) glucometer = true; else glucometer = false;
        if(data[5] == null || data[5].isEmpty()){ glucometerCalib = ""; } else { glucometerCalib = data[5]; }
        if(data[6] == null || data[6].isEmpty()){ otherText = ""; } else { otherText = data[6]; }
    }

    public String[] toStringArray(){

        String[] data = new String[8];

        data[0] = precautions ? "1" : "0";
        data[1] = asepticTech ? "1" : "0";
        data[2] = sharpObjDisp ? "1" : "0";
        data[3] = wasteDisp ? "1" : "0";
        data[4] = glucometer ? "1" : "0";
        if (glucometerCalib == null || glucometerCalib.isEmpty()){ data[5] = ""; } else { data[5] = glucometerCalib; }
        if (otherText == null || otherText.isEmpty()){ data[6] = ""; } else { data[6] = otherText; }

        return data;
    }

    //getters
    public boolean getPrecautions() {
        return precautions;
    }
    public boolean getAsepticTech() {
        return asepticTech;
    }
    public boolean getSharpObjDisp() {
        return sharpObjDisp;
    }
    public boolean getWasteDisp() {
        return wasteDisp;
    }
    public boolean getGlucometer() {
        return glucometer;
    }
    public String getGlucometerCalib() {
        return glucometerCalib;
    }
    public String getOtherText() {
        return otherText;
    }

    //setters
    public void setPrecautions(boolean Precautions) {
        this.precautions = Precautions;
    }
    public void setAsepticTech(boolean asepticTech) {
        this.asepticTech = asepticTech;
    }
    public void setSharpObjDisp(boolean sharpObjDisp) {
        sharpObjDisp = sharpObjDisp;
    }
    public void setWasteDisp(boolean wasteDisp) {
        this.wasteDisp = wasteDisp;
    }
    public void setGlucometer(boolean glucometer) {
        this.glucometer = glucometer;
    }
    public void setGlucometerCalib(String glucometerCalib) {
        this.glucometerCalib = glucometerCalib;
    }
    public void setOtherText(String otherText) {
        this.otherText = otherText;
    }


}
