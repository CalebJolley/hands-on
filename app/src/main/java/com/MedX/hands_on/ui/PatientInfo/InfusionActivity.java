package com.MedX.hands_on.ui.PatientInfo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.MedX.hands_on.R;

import androidx.appcompat.app.AppCompatActivity;

public class InfusionActivity extends AppCompatActivity {

    //variable dec
    CheckBox myIVChange;
    CheckBox myCapChange;
    CheckBox myCatheterChange;
    CheckBox myIVSiteChange;
    EditText myIVSitePrior;
    EditText myIVSitePost;
    EditText myMed;
    EditText myRate;
    EditText myVIA;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.infusion_layout);

        //connect variables to buttons
        myIVChange = findViewById(R.id.ivChangeBox);
        myCapChange = findViewById(R.id.capChangeBox);
        myCatheterChange = findViewById(R.id.catheterSiteChangeBox);
        myIVSiteChange = findViewById(R.id.ivSiteChangeBox);
        myIVSitePrior = findViewById(R.id.ivSitePriorText);
        myIVSitePost = findViewById(R.id.ivSitePostText);
        myMed = findViewById(R.id.medText);
        myRate = findViewById(R.id.rateText);
        myVIA = findViewById(R.id.viaText);


        //load saved state
        Infusion infusion;
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getStringArray("preExistingInfusion") != null){
            infusion = new Infusion();
            infusion.massSet(extras.getStringArray("preExistingInfusion"));
            myIVChange.setChecked(infusion.getIvChange());
            myCapChange.setChecked(infusion.getCapChange());
            myCatheterChange.setChecked(infusion.getCatheterSiteChange());
            myIVSiteChange.setChecked(infusion.getIvSiteChange());
            if (myIVSitePrior != null) {myIVSitePrior.setText(infusion.getIvSitePrior()); }
            if (myIVSitePost != null) {myIVSitePost.setText(infusion.getIvSitePost()); }
            if (myMed != null) {myMed.setText(infusion.getMed()); }
            if (myRate != null) {myRate.setText(infusion.getRate()); }
            if (myVIA != null) {myVIA.setText(infusion.getVia()); }
        }
    }

    public void submitInfusion(View v){
        String[] returnData = new String[9];

        returnData[0] = myIVChange.isChecked() ? "1" : "0";
        returnData[1] = myCapChange.isChecked() ? "1" : "0";
        returnData[2] = myCatheterChange.isChecked() ? "1" : "0";
        returnData[3] = myIVSiteChange.isChecked() ? "1" : "0";
        returnData[4] = myIVSitePrior.getText().toString();
        returnData[5] = myIVSitePost.getText().toString();
        returnData[6] = myMed.getText().toString();
        returnData[7] = myRate.getText().toString();
        returnData[8] = myVIA.getText().toString();

        Intent returnIntent = new Intent();
        returnIntent.putExtra("returnDataInfusion", returnData);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

}
