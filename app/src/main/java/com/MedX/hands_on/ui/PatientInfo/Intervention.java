package com.MedX.hands_on.ui.PatientInfo;

import java.io.Serializable;

public class Intervention implements Serializable {

    //variable dec
    private boolean skilledAssess;
    private boolean foleyChange;
    private boolean irrigation;
    private boolean wound;
    private boolean ulcer;
    private boolean incision;
    private boolean prep;
    private boolean adminInsulin;
    private boolean im;
    private boolean sq;
    private boolean peg;
    private boolean gtCare;
    private boolean diet;
    private boolean medInstruct;
    private boolean ssDiseaseProcess;
    private String otherText;

    public void Intervention(){
        skilledAssess = false;
        foleyChange = false;
        irrigation = false;
        wound = false;
        ulcer = false;
        incision = false;
        prep = false;
        adminInsulin = false;
        im = false;
        sq = false;
        peg = false;
        gtCare = false;
        diet = false;
        medInstruct = false;
        ssDiseaseProcess = false;
        otherText = "";
    }

    public void massSet(String[] data){
        if (data[0].equals("1")) skilledAssess = true; else skilledAssess = false;
        if (data[1].equals("1")) foleyChange = true; else foleyChange = false;
        if (data[2].equals("1")) irrigation = true; else irrigation = false;
        if (data[3].equals("1")) wound= true; else wound = false;
        if (data[4].equals("1")) ulcer = true; else ulcer = false;
        if (data[5].equals("1")) incision = true; else incision = false;
        if (data[6].equals("1")) prep = true; else prep = false;
        if (data[7].equals("1")) gtCare = true; else gtCare = false;
        if (data[8].equals("1")) diet = true; else diet = false;
        if (data[9].equals("1")) adminInsulin = true; else adminInsulin = false;
        if (data[10].equals("1")) im = true; else im = false;
        if (data[11].equals("1")) sq = true; else sq = false;
        if (data[12].equals("1")) peg = true; else peg = false;
        if (data[13].equals("1")) medInstruct = true; else medInstruct = false;
        if (data[14].equals("1")) ssDiseaseProcess = true; else ssDiseaseProcess = false;
        if(data[15] == null || data[15].isEmpty()){ otherText = ""; } else { otherText = data[15]; }
    }

    public String[] toStringArray(){

        String[] data = new String[16];

        data[0] = skilledAssess ? "1" : "0";
        data[1] = foleyChange ? "1" : "0";
        data[2] = irrigation ? "1" : "0";
        data[3] = wound ? "1" : "0";
        data[4] = ulcer ? "1" : "0";
        data[5] = incision ? "1" : "0";
        data[6] = prep ? "1" : "0";
        data[7] = gtCare ? "1" : "0";
        data[8] = diet ? "1" : "0";
        data[9] = adminInsulin ? "1" : "0";
        data[10] = im ? "1" : "0";
        data[11] = sq ? "1" : "0";
        data[12] = peg ? "1" : "0";
        data[13] = medInstruct ? "1" : "0";
        data[14] = ssDiseaseProcess ? "1" : "0";
        if(otherText == null || otherText.isEmpty()){ data[15] = ""; } else { data[15] = otherText; }

        return data;
    }

    //getters
    public boolean getSkilledAssess() {
        return skilledAssess;
    }
    public boolean getFoleyChange() {
        return foleyChange;
    }
    public boolean getIrrigation() {
        return irrigation;
    }
    public boolean getWound() {
        return wound;
    }
    public boolean getUlcer() {
        return ulcer;
    }
    public boolean getIncision() {
        return incision;
    }
    public boolean getPrep() {
        return prep;
    }
    public boolean getAdminInsulin() {
        return adminInsulin;
    }
    public boolean getIm() {
        return im;
    }
    public boolean getSq() {
        return sq;
    }
    public boolean getPeg() {
        return peg;
    }
    public boolean getGtCare(){
        return gtCare;
    }
    public boolean getDiet(){
        return diet;
    }
    public boolean getMedInstruct() {
        return medInstruct;
    }
    public boolean getSsDiseaseProcess() {
        return ssDiseaseProcess;
    }
    public String getOtherText() {
        return otherText;
    }

    //setters
    public void setSkilledAssess(boolean skilledAssess) {
        this.skilledAssess = skilledAssess;
    }
    public void setFoleyChange(boolean foleyChange) {
        this.foleyChange = foleyChange;
    }
    public void setIrrigation(boolean irrigation) {
        this.irrigation = irrigation;
    }
    public void setWound(boolean wound) {
        this.wound = wound;
    }
    public void setUlcer(boolean ulcer) {
        this.ulcer = ulcer;
    }
    public void setIncision(boolean incision) {
        this.incision = incision;
    }
    public void setPrep(boolean prep) {
        this.prep = prep;
    }
    public void setAdminInsulin(boolean adminInsulin) {
        this.adminInsulin = adminInsulin;
    }
    public void setIm(boolean im) {
        this.im = im;
    }
    public void setSq(boolean sq) {
        this.sq = sq;
    }
    public void setPeg(boolean peg) {
        this.peg = peg;
    }
    public void setGtCare(boolean gtCare){
        this.gtCare = gtCare;
    }
    public void setDiet(boolean diet){
        this.diet = diet;
    }
    public void setMedInstruct(boolean medInstruct) {
        this.medInstruct = medInstruct;
    }
    public void setSsDiseaseProcess(boolean ssDiseaseProcess) {
        this.ssDiseaseProcess = ssDiseaseProcess;
    }
    public void setOtherText(String otherText) {
        this.otherText = otherText;
    }

}
