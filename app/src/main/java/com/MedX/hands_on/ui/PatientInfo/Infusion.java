package com.MedX.hands_on.ui.PatientInfo;

public class Infusion {

    //variable dec
    private boolean ivChange;
    private boolean capChange;
    private boolean catheterSiteChange;
    private boolean ivSiteChange;
    private String ivSitePrior;
    private String ivSitePost;
    private String med;
    private String rate;
    private String via;


    public void massSet(String[] data){
        if (data[0].equals("1")) ivChange = true; else ivChange = false;
        if (data[1].equals("1")) capChange = true; else capChange = false;
        if (data[2].equals("1"))  catheterSiteChange= true; else catheterSiteChange = false;
        if (data[3].equals("1")) ivSiteChange= true; else ivSiteChange = false;
        if(data[4] == null || data[4].isEmpty()){ ivSitePrior = ""; } else { ivSitePrior = data[4]; }
        if(data[5] == null || data[5].isEmpty()){ ivSitePost = ""; } else { ivSitePost = data[5]; }
        if(data[6] == null || data[6].isEmpty()){ med = ""; } else { med = data[6]; }
        if(data[7] == null || data[7].isEmpty()){ rate = ""; } else { rate = data[7]; }
        if(data[8] == null || data[8].isEmpty()){ via = ""; } else { via = data[8]; }
    }

    public String[] toStringArray(){

        String[] data = new String[9];

        data[0] = ivChange ? "1" : "0";
        data[1] = capChange ? "1" : "0";
        data[2] = catheterSiteChange ? "1" : "0";
        data[3] = ivSiteChange ? "1" : "0";
        if (ivSitePrior == null || ivSitePrior.isEmpty()){ data[4] = ""; } else { data[4] = ivSitePrior; }
        if (ivSitePost == null || ivSitePost.isEmpty()){ data[5] = ""; } else { data[5] = ivSitePost; }
        if (med == null || med.isEmpty()){ data[6] = ""; } else { data[6] = med; }
        if (rate == null || rate.isEmpty()){ data[7] = ""; } else { data[7] = rate; }
        if (via == null || via.isEmpty()){ data[8] = ""; } else { data[8] = via; }

        return data;
    }

    //getters
    public boolean getIvChange() {
        return ivChange;
    }
    public boolean getCapChange() {
        return capChange;
    }
    public boolean getCatheterSiteChange() {
        return catheterSiteChange;
    }
    public boolean getIvSiteChange() {
        return ivSiteChange;
    }
    public String getIvSitePrior() {
        return ivSitePrior;
    }
    public String getIvSitePost() {
        return ivSitePost;
    }
    public String getMed() {
        return med;
    }
    public String getRate() {
        return rate;
    }
    public String getVia() {
        return via;
    }

    //setters
    public void setIvChange(boolean ivChange) {
        this.ivChange = ivChange;
    }
    public void setCapChange(boolean capChange) {
        this.capChange = capChange;
    }
    public void setCatheterSiteChange(boolean catheterSiteChange) {
        this.catheterSiteChange = catheterSiteChange;
    }
    public void setIvSiteChange(boolean ivSiteChange) {
        this.ivSiteChange = ivSiteChange;
    }
    public void setIvSitePrior(String ivSitePrior) {
        this.ivSitePrior = ivSitePrior;
    }
    public void setIvSitePost(String ivSitePost) {
        this.ivSitePost = ivSitePost;
    }
    public void setMed(String med) {
        this.med = med;
    }
    public void setRate(String rate) {
        this.rate = rate;
    }
    public void setVia(String via) {
        this.via = via;
    }
}
