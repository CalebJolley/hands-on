package com.MedX.hands_on.ui.PatientInfo;

import android.util.Log;

import java.io.Serializable;

public class Cardiovascular implements Serializable {

    private static final String TAG = "Cardiovascular";
    //variables
    private boolean chestPain;
    private boolean rue;
    private boolean lue;
    private boolean rle;
    private boolean lle;
    private boolean abRhythm;
    private boolean pulses;
    private boolean antiCoag;
    private boolean wnl;
    private String otherText;

    public void Cardiovascular(){
        //default to false/empty
        chestPain = false;
        rue = false;
        lue = false;
        rle = false;
        lle = false;
        abRhythm = false;
        pulses = false;
        antiCoag = false;
        wnl = false;
        otherText = "";
    }

    //function used to mass set everything to that of input string array
    public void massSet(String[] data){
        if (data[0].equals("1")) chestPain = true; else chestPain = false;
        if (data[1].equals("1")) rue = true; else rue = false;
        if (data[2].equals("1")) lue = true; else lue = false;
        if (data[3].equals("1")) rle = true; else rle = false;
        if (data[4].equals("1")) lle = true; else lle = false;
        if (data[5].equals("1")) abRhythm = true; else abRhythm = false;
        if (data[6].equals("1")) pulses = true; else pulses = false;
        if (data[7].equals("1")) antiCoag = true; else antiCoag = false;
        if (data[8].equals("1")) wnl = true; else wnl = false;
        if(data[9] == null || data[9].isEmpty()){ otherText = ""; } else { otherText = data[9]; }
        Log.d(TAG, "massSet: Mass set completed successfully");
    }

    public String[] toStringArray(){

        String[] data = new String[10];

        data[0] = chestPain ? "1" : "0";
        data[1] = rue ? "1" : "0";
        data[2] = lue ? "1" : "0";
        data[3] = rle ? "1" : "0";
        data[4] = lle ? "1" : "0";
        data[5] = abRhythm ? "1" : "0";
        data[6] = pulses ? "1" : "0";
        data[7] = antiCoag ? "1" : "0";
        data[8] = wnl ? "1" : "0";
        if(otherText == null || otherText.isEmpty()){ data[9] = ""; } else { data[9] = otherText; }//1, 0, 1, 1, 0, ..., "other text"

        return data;
    }

    //getters
    public boolean getChestPain(){
        return chestPain;
    }
    public boolean getRue(){
        return rue;
    }
    public boolean getLue(){
        return lue;
    }
    public boolean getRle(){
        return rle;
    }
    public boolean getLle(){
        return lle;
    }
    public boolean getAbRhythm(){
        return abRhythm;
    }
    public boolean getPulses(){
        return pulses;
    }
    public boolean getAntiCoag(){
        return antiCoag;
    }
    public boolean getWnl(){
        return wnl;
    }
    public String getOtherText(){
        return otherText;
    }

    //setters
    public void setChestPain(boolean b){
        chestPain = b;
    }
    public void setRue(boolean b){
        rue = b;
    }
    public void setLue(boolean b){
        lue = b;
    }
    public void setRle(boolean b){
        rle = b;
    }
    public void setLle(boolean b){
        lle = b;
    }
    public void setAbRhythm(boolean b){
        abRhythm = b;
    }
    public void setPulses(boolean b){
        pulses = b;
    }
    public void setAntiCoag(boolean b){
        antiCoag = b;
    }
    public void setWnl(boolean b){
        wnl = b;
    }
    public void setOtherText(String text){
        otherText = text;
    }
}
