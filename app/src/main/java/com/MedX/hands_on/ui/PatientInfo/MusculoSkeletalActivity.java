package com.MedX.hands_on.ui.PatientInfo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.MedX.hands_on.R;

import androidx.appcompat.app.AppCompatActivity;

public class MusculoSkeletalActivity extends AppCompatActivity {

    //variable dec
    CheckBox myPoorBalance;
    CheckBox myLimitedMovement;
    CheckBox myChairBound;
    CheckBox myBedBound;
    EditText myWalksWith;
    CheckBox myContracture;
    CheckBox myParalysis;
    CheckBox myWNLMusc;
    EditText myMuscOther;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.musculoskeletal_layout);

        //connect variables to buttons
        myPoorBalance = findViewById(R.id.poorBalanceBox);
        myLimitedMovement = findViewById(R.id.limitedMovementBox);
        myChairBound = findViewById(R.id.chairBoundBox);
        myBedBound = findViewById(R.id.bedBoundBox);
        myWalksWith = findViewById(R.id.walksWithText);
        myContracture = findViewById(R.id.contractureBox);
        myParalysis = findViewById(R.id.paralysisBox);
        myWNLMusc = findViewById(R.id.wnlMuscBox);
        myMuscOther = findViewById(R.id.muscuoSkeletalOtherText);


        //load saved state
        MusculoSkeletal musculoSkeletal;
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getStringArray("preExistingMusc") != null){
            musculoSkeletal = new MusculoSkeletal();
            musculoSkeletal.massSet(extras.getStringArray("preExistingMusc"));
            myPoorBalance.setChecked(musculoSkeletal.getPoorBalance());
            myLimitedMovement.setChecked(musculoSkeletal.getLimitedMovement());
            myChairBound.setChecked(musculoSkeletal.getChairBound());
            myBedBound.setChecked(musculoSkeletal.getBedBound());
            if(myWalksWith != null){ myWalksWith.setText(musculoSkeletal.getWalksWith()); }
            myContracture.setChecked(musculoSkeletal.getContracture());
            myParalysis.setChecked(musculoSkeletal.getParalysis());
            myWNLMusc.setChecked(musculoSkeletal.getWNL());
            if(myMuscOther != null){ myMuscOther.setText(musculoSkeletal.getOtherText()); }
        }
    }

    public void submitMusculoSkeletal(View v){
        String[] returnData = new String[9];
        returnData[0] = myPoorBalance.isChecked() ? "1" : "0";
        returnData[1] = myLimitedMovement.isChecked() ? "1" : "0";
        returnData[2] = myChairBound.isChecked() ? "1" : "0";
        returnData[3] = myBedBound.isChecked() ? "1" : "0";
        returnData[4] = myWalksWith.getText().toString();
        returnData[5] = myContracture.isChecked() ? "1" : "0";
        returnData[6] = myParalysis.isChecked() ? "1" : "0";
        returnData[7] = myWNLMusc.isChecked() ? "1" : "0";
        returnData[8] = myMuscOther.getText().toString();

        Intent returnIntent = new Intent();
        returnIntent.putExtra("returnDataMusc", returnData);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }


}
