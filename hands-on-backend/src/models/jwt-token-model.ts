export class JwtTokenModel{
    firstname?: string;
    lastname?: string;
    email?: string;
    phone?: number|null = null;
    amount?: number|null = null;
    id?: object;
    expireTime: number = Date.now() + 3600 * 1000;
}
