import mongoose from 'mongoose';

var authorityGroups = ["DOCTOR", "NURSE", "ASSISTANT"];
var gender = ["MALE", "FEMALE", "OTHER"];
var department = ["OFFICE"];

var employeeSchema = new mongoose.Schema({
    authorityGroup: {
        type: String,
        enum: authorityGroups,
        require: true,
    },
    firstname: String,
    lastname: String,
    patients: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'patient'
    }],
    fromDate: Date,
    dateOfBirth: Date,
    gender: {
        type: String,
        enum: gender
    },
    supervisorId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'employee'
    },
    department: {
        type: String,
        enum: department
    },
    email: String,
    phone: String,
    password: String
});

var EmployeeMongooseModel = mongoose.model('employee', employeeSchema);

export { EmployeeMongooseModel };