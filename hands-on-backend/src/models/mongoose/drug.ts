import mongoose from 'mongoose';

var drugSchema = new mongoose.Schema({
    rxcui: String,
    name: String,
    category: String,
    mfg: Date
});

var DrugMongooseModel = mongoose.model('drug', drugSchema);

export { DrugMongooseModel };