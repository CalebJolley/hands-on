import mongoose from 'mongoose';

var gender = ["MALE", "FEMALE", "OTHER"];

var patientSchema = new mongoose.Schema({
    firstname: String,
    lastname: String,
    email: String,
    phone: String,
    dateOfBirth: Date,
    gender: {
        type: String,
        enum: gender
    },
    address: String,
    memo: String
});

var PatientMongooseModel = mongoose.model('patient', patientSchema);

export { PatientMongooseModel };