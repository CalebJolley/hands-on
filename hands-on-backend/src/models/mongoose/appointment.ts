import mongoose from 'mongoose';

var frequency = ["ONETIME", "RECURRING-7", "RECURRING-30"]; // Recurring-DAYS
var status = ["CANCELLED", "PENDING", "SUCCESS"]; // Recurring-DAYS

var appointmentSchema = new mongoose.Schema({
    patientId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'patient'
    },
    employeeId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'employee'
    },
    date: Date,
    frequencyInfo: {
        type: String,
        enum: frequency
    },
    diagnosis: String,
    treatment: String,
    prescriptionDrugs: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'drug'
    }],
    memo: String,
    status: {
        type: String,
        enum: status
    }
});

var AppointmentMongooseModel = mongoose.model('appointment', appointmentSchema);

export { AppointmentMongooseModel };
