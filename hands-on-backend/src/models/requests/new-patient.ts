export var newPatientRequestModel = {
    firstname: String,
    lastname: String,
    email: String,
    phone: String,
    dateOfBirth: Date,
    gender: String,
    address: String,
    $optionals: {
        memo: String
    }
}


