export var newDrugRequestModel = {
    rxcui: String,
    name: String,
    category: String,
    mfg: Date
};
