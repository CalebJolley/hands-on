export var newAppointmentRequestModel = {
    patientId: String,
    employeeId: String,
    date: Date,
    frequencyInfo: String,
    diagnosis: String,
    treatment: String,
    prescriptionDrugs: [String],
    memo: String,
    status: String
}
