export var newEmployeeRequestModel = {
    authorityGroup: String,
    firstname: String,
    lastname: String,
    fromDate: Date,
    dateOfBirth: Date,
    gender: String,
    phone: String,
    password: String,
    email: String,
    $optionals:{    
        supervisorId: String,
        department: String,
    }
}


