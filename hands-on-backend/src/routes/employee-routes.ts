import express from 'express';
import {EmployeeController as Controller} from "../controllers/employee-controllers";
import isAuthorized from '../middlewares/is-authorized';

const router = express.Router();

router.get('/',
    isAuthorized,
    Controller.get
);

router.post('/',
    isAuthorized,
    Controller.post
);

router.delete('/',
    isAuthorized,
    Controller.delete
);

router.put('/',
    isAuthorized,
    Controller.put
);

router.put('/add-patient',
    isAuthorized,
    Controller.addPatient
);

router.delete('/remove-patient',
    isAuthorized,
    Controller.removePatient
);

export default router;