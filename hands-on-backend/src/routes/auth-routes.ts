import express from 'express';
import {AuthControllers as Controller} from "../controllers/auth-controllers";

const router = express.Router();

router.get('/',
    Controller.get
);

router.post('/',
    Controller.post
);

router.delete('/',
    Controller.delete
);

router.put('/',
    Controller.put
);

export default router;