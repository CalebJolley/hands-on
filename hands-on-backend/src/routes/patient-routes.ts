import express from 'express';
import {PatientController as Controller} from "../controllers/patient-controllers";
import isAuthorized from '../middlewares/is-authorized';

const router = express.Router();

router.get('/',
    isAuthorized,
    Controller.get
);

router.get('/history',
    isAuthorized,
    Controller.getHistory
);

router.post('/',
    isAuthorized,
    Controller.post
);

router.delete('/',
    isAuthorized,
    Controller.delete
);

router.put('/',
    isAuthorized,
    Controller.put
);

export default router;