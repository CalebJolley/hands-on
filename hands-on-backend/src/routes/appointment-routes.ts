import express from 'express';
import {AppointmentController as Controller} from "../controllers/appointment-controllers";
import isAuthorized from '../middlewares/is-authorized';

const router = express.Router();

router.get('/',
    isAuthorized,
    Controller.get
);

router.post('/',
    isAuthorized,
    Controller.post
);

router.delete('/',
    isAuthorized,
    Controller.delete
);

router.put('/',
    isAuthorized,
    Controller.put
);

export default router;