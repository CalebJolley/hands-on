import express from 'express';
import {DrugController as Controller} from "../controllers/drug-controllers";
import isAuthorized from '../middlewares/is-authorized';

const router = express.Router();

router.get('/',
    isAuthorized,
    Controller.get
);

router.post('/',
    isAuthorized,
    Controller.post
);

router.delete('/',
    isAuthorized,
    Controller.delete
);

router.put('/',
    isAuthorized,
    Controller.put
);

export default router;