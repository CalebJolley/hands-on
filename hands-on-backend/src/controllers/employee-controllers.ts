import { newEmployeeRequestModel } from "../models/requests/new-employee";
import { EmployeeMongooseModel } from "../models/mongoose";

export class EmployeeController {
    public static post = (req, res, next) => {
        var newEmployeeObject = req.helpers.mapProperties(req.body, newEmployeeRequestModel);

        return new EmployeeMongooseModel(newEmployeeObject).save().then((employeeObject) => {
            res.json({
                status: 200,
                _id: employeeObject._id.toString(),
                err: ""
            }).end();
        });
    }

    public static get = (req, res, next) => {
        let query = {};
        if (req.query?._id) {
            query = { _id: req.query._id };
        }
        else if (req.query?.email) {
            query = { email: req.query.email };
        }
        else if (req.query?.phone) {
            query = { phone: req.query.phone };
        }
        else if (req.query?.query) {
            query = JSON.parse(req.query.query);
        }

        EmployeeMongooseModel.find(query).then(documents => {
            var employees: Array<any> = [];

            documents.forEach(document => {
                var doc = document as any;
                employees.push({
                    _id: doc._id.toString(),
                    authorityGroup: doc.authorityGroup,
                    firstname: doc.firstname,
                    lastname: doc.lastname,
                    fromDate: doc.fromDate,
                    dateOfBirth: doc.dateOfBirth,
                    gender: doc.gender,
                    phone: doc.phone,
                    email: doc.email,
                    supervisorId: doc.supervisorId?.toString(),
                    department: doc.department,
                    patients: doc.patients.map(element => element.toString())
                });
            });

            res.json({
                status: 200,
                employees: employees,
                err: ""
            }).end();
        })
    }

    public static delete = (req, res, next) => {
        if (req.body?._id) {
            EmployeeMongooseModel.findByIdAndDelete(req.body._id)
                .then(document => {
                    var doc = document as any;

                    return res.json({
                        status: 200,
                        _id: doc._id.toString(),
                        err: ""
                    }).end();
                })
                .catch(err => {
                    var error = new Error("Not found");
                    error.status = 404;
                    next(error);
                })
        }
        else {
            var error = new Error("Not found");
            error.status = 404;
            next(error);
        }
    }

    public static put = (req, res, next) => {
        var err = new Error('Not supported');
        err['status'] = 400;
        next(err);
    }

    public static addPatient = (req, res, next) => {
        if (req.body?._id) {
            EmployeeMongooseModel.updateOne(
                { _id: req.body?._id },
                { $addToSet: { patients: req.body?.patientId } },
                (err, success) => {
                    if (err) {
                        var error = new Error("Not found");
                        error.status = 404;
                        next(error);
                    } else {
                        res.json({
                            status: 200,
                            message: "Modified " + success.nModified + " records",
                            err: ""
                        }).end();
                    }
                    console.log(success);
                });
        } else {
            var error = new Error("Not found");
            error.status = 404;
            next(error);
        }
    }

    public static removePatient = (req, res, next) => {
        if (req.body?._id) {
            EmployeeMongooseModel.updateOne(
                { _id: req.body?._id },
                { $pull: { patients: req.body?.patientId } },
                (err, success) => {
                    if (err) {
                        var error = new Error("Not found");
                        error.status = 404;
                        next(error);
                    } else {
                        res.json({
                            status: 200,
                            message: "Modified " + success.nModified + " records",
                            err: ""
                        }).end();
                    }
                    console.log(success);
                });
        } else {
            var error = new Error("Not found");
            error.status = 404;
            next(error);
        }
    }
}
