import { newPatientRequestModel } from "../models/requests/new-patient";
import { PatientMongooseModel, AppointmentMongooseModel } from "../models/mongoose";

export class PatientController {
    public static post = (req, res, next) => {
        var newPatientObject = req.helpers.mapProperties(req.body, newPatientRequestModel);

        return new PatientMongooseModel(newPatientObject).save().then((patientObject) => {
            res.json({
                status: 200,
                _id: patientObject._id.toString(),
                err: ""
            }).end();
        });
    }

    public static get = (req, res, next) => {
        let query = {};
        if (req.query?._id) {
            query = { _id: req.query._id };
        }
        else if (req.query?.email) {
            query = { email: req.query.email };
        }
        else if (req.query?.phone) {
            query = { phone: req.query.phone };
        }
        else if (req.query?.query) {
            query = JSON.parse(req.query.query);
        }

        PatientMongooseModel.find(query).then(documents => {
            var patients: Array<any> = [];

            documents.forEach(document => {
                var doc = document as any;
                patients.push({
                    _id: doc._id.toString(),
                    firstname: doc.firstname,
                    lastname: doc.lastname,
                    email: doc.email,
                    phone: doc.phone,
                    dateOfBirth: doc.dateOfBirth,
                    gender: doc.gender,
                    address: doc.address,
                    memo: doc.memo
                });
            });

            res.json({
                status: 200,
                patients: patients,
                err: ""
            }).end();
        })
    }

    public static getHistory = (req, res, next) => {
        let query = {};
        if (req.query?.patientId) {
            query = { $and: [{ patientId: req.query.patientId }, { status: "SUCCESS" }] };
        }
        else if (req.query?.email) {
            query = { $and: [{ "patientId.email": req.query.email }, { status: "SUCCESS" }] };
        }
        else if (req.query?.phone) {
            query = { $and: [{ "patientId.phone": req.query.phone }, { status: "SUCCESS" }] };
        }
        else if (req.query?.query) {
            query = JSON.parse(req.query.query);
        }

        AppointmentMongooseModel.find(query).then(documents => {
            var records: Array<any> = [];

            documents.forEach(document => {
                var doc = document as any;
                records.push({
                    date: doc.date,
                    patientId: doc.patientId.toString(),
                    employeeId: doc.employeeId.toString(),
                    frequencyInfo: doc.frequencyInfo,
                    diagnosis: doc.diagnosis,
                    treatment: doc.treatment,
                    prescriptionDrugs: doc.prescriptionDrugs,
                    memo: doc.memo,
                    status: doc.status
                });
            });

            res.json({
                status: 200,
                records: records,
                err: ""
            }).end();
        })
    }

    public static delete = (req, res, next) => {
        if (req.body?._id) {
            PatientMongooseModel.findByIdAndDelete(req.body._id)
                .then(document => {
                    var doc = document as any;

                    return res.json({
                        status: 200,
                        _id: doc._id.toString(),
                        err: ""
                    }).end();
                })
                .catch(err => {
                    var error = new Error("Not found");
                    error.status = 404;
                    next(error);
                })
        }
        else {
            var error = new Error("Not found");
            error.status = 404;
            next(error);
        }
    }

    public static put = (req, res, next) => {
        var err = new Error('Not supported');
        err['status'] = 400;
        next(err);
    }
}
