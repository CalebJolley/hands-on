import { newDrugRequestModel } from "../models/requests/new-drug";
import { DrugMongooseModel } from "../models/mongoose";

export class DrugController {
    public static post = (req, res, next) => {
        var newDrugObject = req.helpers.mapProperties(req.body, newDrugRequestModel);

        return new DrugMongooseModel(newDrugObject).save().then((drugObject) => {
            res.json({
                status: 200,
                _id: drugObject._id.toString(),
                err: ""
            }).end();
        });
    }

    public static get = (req, res, next) => {
        let query = {};
        if (req.query?._id) {
            query = { _id: req.query._id };
        }
        else if (req.query?.rxcui) {
            query = { rxcui: req.query.rxcui };
        }
        else if (req.query?.mfg) {
            query = { mfg: req.query.mfg };
        }
        else if (req.query?.category) {
            query = { category: req.query.category };
        }
        else if (req.query?.name) {
            query = { name: req.query.name };
        }
        else if (req.query?.query) {
            query = JSON.parse(req.query.query);
        }
        
        DrugMongooseModel
            .find(query).then(documents => {
                var drugs: Array<any> = [];

                documents.forEach(document => {
                    var doc = document as any;
                    drugs.push({
                        _id: doc._id.toString(),
                        rxcui: doc.rxcui,
                        name: doc.name,
                        category: doc.category,
                        mfg: doc.mfg
                    });
                });

                res.json({
                    status: 200,
                    drugs: drugs,
                    err: ""
                }).end();
            })
            .catch(err => {
                var error = new Error("Not found");
                error.status = 404;
                next(error);
            })
    }

    public static delete = (req, res, next) => {
        if (req.body?._id) {
            DrugMongooseModel.findByIdAndDelete(req.body._id)
                .then(document => {
                    var doc = document as any;

                    return res.json({
                        status: 200,
                        _id: doc._id.toString(),
                        err: ""
                    }).end();
                })
                .catch(err => {
                    var error = new Error("Not found");
                    error.status = 404;
                    next(error);
                })
        }
        else {
            var error = new Error("Not found");
            error.status = 404;
            next(error);
        }
    }

    public static put = (req, res, next) => {
        var err = new Error('Not supported');
        err['status'] = 400;
        next(err);
    }
}
