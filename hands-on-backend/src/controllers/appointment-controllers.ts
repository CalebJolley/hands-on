import { AppointmentMongooseModel } from '../models/mongoose';
import { Request, Response, NextFunction } from 'express-serve-static-core';
import { newAppointmentRequestModel } from '../models/requests/new-appointment';

export class AppointmentController {
    public static post = async (req: Request, res: Response, next: NextFunction) => {
        var newAppointmentObject = req.helpers.mapProperties(req.body, newAppointmentRequestModel);

        return new AppointmentMongooseModel(newAppointmentObject).save().then((appointmentObject) => {
            res.json({
                status: 200,
                _id: appointmentObject._id,
                err: ""
            }).end();
        });
    }

    public static get = async (req: Request, res: Response, next: NextFunction) => {
        var query: object;

        if (req.query._id) {
            query = { _id: req.query._id };
        }
        else if (req.query.employeeId) {
            query = { employeeId: req.query.employeeId };
        }
        else if (req.query.patient) {
            query = { patientId: req.query.patientId };
        }
        else if (req.query.query) {
            query = JSON.parse(req.query.query);
        }
        else {
            var err = new Error("One of _id, employee, or patient shouldn't be null");
            err.status = 400;
            return next(err);
        }

        AppointmentMongooseModel.find(query).then(documents => {
            var appointments: Array<any> = [];

            documents.forEach(document => {
                var doc = document as any;
                appointments.push({
                    _id: doc._id.toString(),
                    patientId: doc.patientId,
                    employeeId: doc.employeeId,
                    date: doc.date,
                    frequencyInfo: doc.frequencyInfo,
                    diagnosis: doc.diagnosis,
                    treatment: doc.treatment,
                    prescriptionDrugs: doc.prescriptionDrugs.map(ele => ele.toString()),
                    memo: doc.memo,
                    status: doc.status
                });
            });

            res.json({
                status: 200,
                appointments: appointments,
                err: ""
            }).end();
        });
    }

    public static delete = (req: Request, res: Response, next: NextFunction) => {
        if (req.body?._id) {
            AppointmentMongooseModel.findByIdAndDelete(req.body._id)
                .then(document => {
                    var doc = document as any;

                    return res.json({
                        status: 200,
                        _id: doc._id.toString(),
                        err: ""
                    }).end();
                })
                .catch(err => {
                    var error = new Error("Not found");
                    error.status = 404;
                    next(error);
                })
        }
        else {
            var error = new Error("Not found");
            error.status = 404;
            next(error);
        }
    }

    public static put = (req: Request, res: Response, next: NextFunction) => {
        if (req.body?._id) {
            AppointmentMongooseModel.findOneAndUpdate({_id: req.body._id}, req.body.updatedFields)
                .then(document => {
                    var doc = document as any;

                    return res.json({
                        status: 200,
                        _id: doc._id.toString(),
                        err: ""
                    }).end();
                })
                .catch(err => {
                    var error = new Error("Not found");
                    error.status = 404;
                    next(error);
                })
        }
        else {
            var error = new Error("Not found");
            error.status = 404;
            next(error);
        }
    }
}
