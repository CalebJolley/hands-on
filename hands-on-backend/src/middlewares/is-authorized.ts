import jwt from 'jsonwebtoken';
import { JwtTokenModel } from '../models/jwt-token-model';
import { Request, Response, NextFunction } from 'express';
import { config } from '../utils/config';

export default (req: Request, res: Response, next: NextFunction) => {
    return next();

    var token: string;

    // read from Authorization header
    if (req.get('Authorization')) {
        req.log("Authorized using bearer");
        token = req.get('Authorization')?.split(' ')[1] || "";	// Bearer TOKEN
    }
    // or read from cookies
    else if (req.cookies?.jwtToken) {
        req.log("Authorized using cookie");
        token = req.cookies.jwtToken;
    }
    // can't authorize
    else {
        const error = new Error('Not authenticated');
        error.status = 401;
        throw error;
    }

    var decodedToken: JwtTokenModel;

    try {
        decodedToken = jwt.verify(token, config.jwtKey) as JwtTokenModel;

        if (decodedToken?.expireTime < (Date.now() - 3600 * 1000)) {
            const error = new Error('Could not authenticate. Token expired.');
            error.status = 401;
            throw error;
        }
    }
    catch (err) {
        if (!err.status) {
            err.status = 500;
        }
        throw err;
    }

    if (!decodedToken) {
        const error = new Error('Could not authenticate.');
        error.status = 401;
        throw error;
    }

    req.jwt = decodedToken;

    next();
};