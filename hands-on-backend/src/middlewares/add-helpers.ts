﻿import * as utils from '../utils/helpers';
import { Request, Response, NextFunction } from 'express';

// attach all function of util to req.helpers
export default (req: Request, res: Response, next: NextFunction) => {
    req.helpers = utils;
    req.log = req.helpers.log;  // just a shortcut
    next();
}