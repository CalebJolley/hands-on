import * as tempConfig from '../config.json';

export var config = tempConfig[process.env.NODE_ENV || "production"];