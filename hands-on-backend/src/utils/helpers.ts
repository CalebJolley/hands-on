export function safe(value) {
    return (typeof (value) != 'undefined') ? value : 'undefined';
}

export function log() {
    console.log(...arguments);
}

export function mapProperties(from: any, to: any, strict: boolean = true) {
    var outputModel = {};

    for (var prop in to) {
        if (prop[0] !== '$' && Object.prototype.hasOwnProperty.call(to, prop)) {
            if (strict && from[prop] === undefined) {
                var err = new Error(prop + " value not provided.");
                err['status'] = 400;
                throw err;
            }

            outputModel[prop] = from[prop];
        }
    }

    if (Object.prototype.hasOwnProperty.call(to, '$optional')) {
        for (var prop in to.$optional) {
            if (Object.prototype.hasOwnProperty.call(to.$optional, prop) && !(strict && from[prop] === undefined)) {
                outputModel[prop] = from[prop];
            }
        }
    }

    return outputModel;
}

