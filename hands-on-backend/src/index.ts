import express from "express";
import debug from "debug";
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import mongoose from 'mongoose';

import helpers from './middlewares/add-helpers';

import employeeRoutes from './routes/employee-routes';
import patientRoutes from './routes/patient-routes';
import apointmentRoutes from './routes/appointment-routes';
import drugRoutes from './routes/drug-routes';
import authRoutes from './routes/auth-routes';

import { config } from './utils/config';
import { Request, Response, NextFunction } from "express-serve-static-core";

var app = express();

//#region Middlewares
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(helpers);
//#endregion

//#region Access-Control-Allow-* Headers
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,DELETE,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-client-key, x-client-token, x-client-secret, Authorization");
    next();
});
//#endregion

//#region Routes 
app.use('/auth', authRoutes);
app.use('/employee', employeeRoutes);
app.use('/patient', patientRoutes);
app.use('/appointment', apointmentRoutes);
app.use('/drug', drugRoutes);
//#endregion

//#region Error handlers
// catch 404 and forward to error handler
app.use((req: Request, res: Response, next: NextFunction) => {
    var err = new Error('Not Found');
    err['status'] = 404;
    next(err);
});

// development error handler, will print stacktrace
if (app.get('env') === 'development') {
    app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
        console.log(req.originalUrl);
        console.log(err.message, err);

        res.status(err['status'] || 500);
        res.json({
            message: err.message,
            status: err.status,
            error: err,
            env: "development"
        });
    });
}

// production error handler, no stacktraces leaked to user
app.use((err: any, req: Request, res: Response, next: NextFunction) => {
    console.log(err.message, err);

    res.status(err.status || 500);
    res.json({
        message: err.message,
        status: err.status,
        error: err,
        env: "production"
    });
});
//#endregion

//#region Server
app.set('port', process.env.PORT || 8081);

mongoose.set('useCreateIndex', true);
mongoose
    .connect(config.mongoDb, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        var server = app.listen(
            app.get('port'),
            () => {
                debug('Express server listening on port ' + app.get('port'));
                console.log('Express server listening on port ' + app.get('port') + '...');
            });
    });
//#endregion
