import { JwtTokenModel } from "../../src/models/jwt-token-model";

declare global{
    namespace Express {
        interface Request {
            jwt: JwtTokenModel;
            helpers: any;
            log: any;
        }
    }
}